package br.com.technical.evaluation.ibm.user_vote.service;

import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteForListDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteSaveDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteUpdateDto;
import br.com.technical.evaluation.ibm.user_vote.exceptions.CpfDontHavePermissionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.ExpiredVotingSessionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserHasVotedException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserVoteNotFoundException;

import java.util.List;
import java.util.Optional;

public interface UserVoteService {

    Optional<UserVoteDto> save(String userCpfLogin, UserVoteSaveDto userVoteSaveDto)
            throws ExpiredVotingSessionException,
                   RulingNotFoundException,
                   UserCpfLoginNotFoundException,
                   UserHasVotedException,
                   CpfDontHavePermissionException;

    Optional<UserVoteDto> getByCpfLoginAndRulingId(String userCpfLogin, Integer rulingId)
            throws UserVoteNotFoundException;

    List<UserVoteForListDto> listAllByRulingId(Integer rulingId);

    Optional<UserVoteUpdateDto> update(String userCpfLogin, UserVoteUpdateDto userVoteUpdateDto)
            throws UserVoteNotFoundException, CpfDontHavePermissionException;

    void removeByCpfLoginAndRulingId(String userCpfLogin, Integer rulingId) throws UserVoteNotFoundException;
}
