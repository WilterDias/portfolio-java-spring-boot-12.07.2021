package br.com.technical.evaluation.ibm.user_vote.dtos;

public enum CpfStatusEnum {
    ABLE_TO_VOTE(Constants.ABLE_TO_VOTE),
    UNABLE_TO_VOTE(Constants.UNABLE_TO_VOTE);

    private static final CpfStatusEnum[] values = CpfStatusEnum.values();

    private static class Constants {
        public static final String ABLE_TO_VOTE   = "ABLE_TO_VOTE";
        public static final String UNABLE_TO_VOTE = "UNABLE_TO_VOTE";
    }

    private final String label;

    private CpfStatusEnum(String label) {
        this.label = label;
    }

    public static CpfStatusEnum getById(int id) {
        return values[id];
    }

    public String getLabel(){
        return this.label;
    }
}
