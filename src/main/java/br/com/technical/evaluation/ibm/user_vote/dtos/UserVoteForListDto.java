package br.com.technical.evaluation.ibm.user_vote.dtos;

import br.com.technical.evaluation.ibm.user_vote.model.UserVote;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserVoteForListDto {
    //@CPF(message = "{error.userCpfLogin}")//TODO Criar uma annotation para verificar o CPF.
    @NotBlank(message = "{error.userVoteCpfLoginBlank}")
    private String cpfLogin;
    @NotNull(message = "{error.userVoteRulingId}")
    private Integer rulingId;
    @NotNull(message = "{error.userVoteVoteYes}")
    private Boolean voteIsYes;

    public static UserVoteForListDto userVoteToUserVoteForListDto(UserVote userVote) {
        return UserVoteForListDto.builder()
                                 .rulingId(userVote.getIdentificator())
                                 .voteIsYes(userVote.getVoteIsYes())
                                 .build();
    }
}
