package br.com.technical.evaluation.ibm.user.controller;

import br.com.technical.evaluation.ibm.config.JwtRoleTypeEnum;
import br.com.technical.evaluation.ibm.config.JwtServiceConfig;
import br.com.technical.evaluation.ibm.user.dtos.UserDto;
import br.com.technical.evaluation.ibm.user.dtos.UserSaveDto;
import br.com.technical.evaluation.ibm.user.dtos.UserUpdateDto;
import br.com.technical.evaluation.ibm.user.exceptions.UnauthorizedOperationException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginExistException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(value="${api.users}")
@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/users")
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "${api.users.getByCpfLogin}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/{userCpfLogin}")
    public Optional<UserDto> getByCpfLogin(@PathVariable String userCpfLogin) throws UserCpfLoginNotFoundException {
        return this.userService.getByCpfLogin(userCpfLogin);
    }

    @ApiOperation(value = "${api.users.listAll}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/list")
    public List<UserDto> listAll() {
        return this.userService.listAll();
    }

    //É colocado um usuário padrão com ROLE_ADMIN quando se implanta a aplicação, olhar nos migrations do flyway.
    //Somente usuários com a ROLE_ADMIN podem atualizar usuários com a ROLE_ADMIN.
    //Usuários com a ROLE_USER podem atualizar a eles próprios.
    @ApiOperation(value = "${api.users.update}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_USER, JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @PutMapping(value = "/")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Optional<UserDto> update(
            @Valid @RequestBody UserUpdateDto userUpdateDto,
            @AuthenticationPrincipal User user
    ) throws UserCpfLoginNotFoundException, UnauthorizedOperationException {
        if (!userIsAdmin(user)
            && (!userUpdateDto.getCpfLogin().equals(user != null? user.getUsername(): "")
                    || JwtRoleTypeEnum.Constants.ROLE_ADMIN.equals(userUpdateDto.getRole().getLabel()))) {
            throw new UnauthorizedOperationException();
        }
        return this.userService.update(userUpdateDto);
    }

    //Somente usuários com a ROLE_ADMIN podem deletar usuários com a ROLE_ADMIN.
    //Usuários com a ROLE_ADMIN podem remover qualquer usuário
    //Usuários com a ROLE_USER podem remover a eles próprios.
    @ApiOperation(value = "${api.users.remove}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_USER, JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @DeleteMapping(value = "/{userCpfLogin}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable String userCpfLogin, @AuthenticationPrincipal User user)
            throws UserCpfLoginNotFoundException, UnauthorizedOperationException {
        if (!userCpfLogin.equals(user.getUsername())
                && !userIsAdmin(user)) {
            throw new UnauthorizedOperationException();
        }
        this.userService.remove(userCpfLogin); // no content
    }

    //É colocado um usuário padrão com ROLE_ADMIN quando se implanta a aplicação, olhar nos migrations do flyway.
    //Somente usuários com a ROLE_ADMIN podem criar novos usuários com a ROLE_ADMIN.
    //Sem necessidade de segurança para a criação de usuários com a ROLE_USER.
    @ApiOperation(value = "${api.users.save}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_USER,
              JwtRoleTypeEnum.Constants.ROLE_ADMIN,
              JwtRoleTypeEnum.Constants.ROLE_ANONYMOUS})
    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody UserSaveDto userSaveDto, @AuthenticationPrincipal User user)
            throws UserCpfLoginExistException, UnauthorizedOperationException {
        if (JwtRoleTypeEnum.Constants.ROLE_ADMIN.equals(userSaveDto.getRole().getLabel())
                && !userIsAdmin(user)) {
            throw new UnauthorizedOperationException();
        }
        userService.save(userSaveDto);
    }

    private boolean userIsAdmin (User user) {
        if (user == null) return false;
        return user.getAuthorities().contains(JwtServiceConfig.AUTHORITY_ADMIN);
    }

}