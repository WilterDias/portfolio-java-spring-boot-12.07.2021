package br.com.technical.evaluation.ibm.ruling.repository;

import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RulingRepository extends JpaRepository<Ruling, Integer> {
}
