package br.com.technical.evaluation.ibm.ruling.controller;

import br.com.technical.evaluation.ibm.config.JwtRoleTypeEnum;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingSaveDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingUpdateDto;
import br.com.technical.evaluation.ibm.ruling.exceptions.PlacedTimeIntervalIsExceededException;
import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;
import br.com.technical.evaluation.ibm.ruling.service.RulingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(value="${api.rulings}")
@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/rulings")
public class RulingController {
    private final RulingService rulingService;

    @ApiOperation(value = "${api.rulings.getById}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_USER, JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/{rulingId}")
    public Optional<RulingDto> getById(@PathVariable Integer rulingId) throws RulingNotFoundException {
        return this.rulingService.getById(rulingId);
    }

    @ApiOperation(value = "${api.rulings.listAll}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_USER, JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/list")
    public List<RulingDto> listAll() {
        return this.rulingService.listAll();
    }

    @ApiOperation(value = "${api.rulings.update}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @PutMapping(value = "/")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Optional<RulingDto> update(
            @Valid @RequestBody RulingUpdateDto rulingUpdateDto) throws RulingNotFoundException {
        return this.rulingService.update(rulingUpdateDto);
    }

    @ApiOperation(value = "${api.rulings.remove}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @DeleteMapping(value = "/{rulingId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer rulingId) throws RulingNotFoundException {
        this.rulingService.remove(rulingId); // no content
    }

    @ApiOperation(value = "${api.rulings.save}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public Optional<RulingDto> save(@RequestBody RulingSaveDto rulingSaveDto)
            throws PlacedTimeIntervalIsExceededException {
        return rulingService.save(rulingSaveDto);
    }
}
