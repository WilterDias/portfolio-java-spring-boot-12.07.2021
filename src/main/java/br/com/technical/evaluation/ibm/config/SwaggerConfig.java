package br.com.technical.evaluation.ibm.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;
import java.util.Collections;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
@PropertySource(value = "classpath:i18n/swagger.messages.properties", encoding = "UTF-8")
class SwaggerConfig {
    @Value("${project.version}")
    private String version;
    @Value("${user.swagger.baseurl}")
    private String baseUrl;

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }
    @Bean
    public Docket api(ServletContext servletContext) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("public-api")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                //.paths(not(regex("/error.*")))
                .build()
                .pathProvider(new RelativePathProvider(servletContext) {
                    @Override
                    public String getApplicationBasePath() {
                        return baseUrl;
                    }
                })
                //.securityContexts(Lists.newArrayList(securityContext()))
                .securitySchemes(Collections.singletonList(apiKey()))
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Swagger API")
                .description("Documentação das APIs")
                .termsOfServiceUrl("http://127.0.0.1")
                .contact(new Contact("nome",
                        "url",
                        "any@gmail.com"))
                .license("License")
                .licenseUrl("any@gmail.com")
                .version(version).build();
    }
}