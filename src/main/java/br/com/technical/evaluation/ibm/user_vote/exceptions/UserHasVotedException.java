package br.com.technical.evaluation.ibm.user_vote.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "exception.userHasVotedException")
public class UserHasVotedException extends Exception {}
