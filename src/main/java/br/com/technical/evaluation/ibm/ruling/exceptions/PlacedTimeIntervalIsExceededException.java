package br.com.technical.evaluation.ibm.ruling.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "exception.placedTimeIntervalIsExceededException")
public class PlacedTimeIntervalIsExceededException extends Exception {}
