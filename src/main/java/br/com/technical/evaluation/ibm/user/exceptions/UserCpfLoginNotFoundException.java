package br.com.technical.evaluation.ibm.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "exception.userCpfLoginNotFoundException")
public class UserCpfLoginNotFoundException extends Exception {}
