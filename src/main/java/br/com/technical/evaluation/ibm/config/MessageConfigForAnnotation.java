package br.com.technical.evaluation.ibm.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
class MessageConfigForAnnotation {

    @Bean
    public MessageSource annotationMessageSource() {
        var messageResource = new ReloadableResourceBundleMessageSource();
        messageResource.setBasename("classpath:i18n/annotation.messages");
        messageResource.setDefaultEncoding("UTF-8");
        messageResource.setCacheSeconds(3600);
        messageResource.setUseCodeAsDefaultMessage(false);
        return messageResource;
    }

    @Bean
    LocalValidatorFactoryBean getValidator() {
        var bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(annotationMessageSource());
        return bean;
    }
}