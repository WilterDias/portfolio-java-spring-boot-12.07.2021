package br.com.technical.evaluation.ibm.config;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "jwt")
@Configuration
@Service
public class JwtServiceConfig {

    private String secret;
    
    private Integer expirationDateInMs;
    
    private Integer restoreExpirationDateInMs;

    public static final SimpleGrantedAuthority AUTHORITY_USER  = new SimpleGrantedAuthority(JwtRoleTypeEnum.ROLE_USER.getLabel());
    public static final SimpleGrantedAuthority AUTHORITY_ADMIN = new SimpleGrantedAuthority(JwtRoleTypeEnum.ROLE_ADMIN.getLabel());
    private static final String IS_USER  = "isUser";
    private static final String IS_ADMIN = "isAdmin";
    private static final String INVALID_CREDENTIALS = "INVALID_CREDENTIALS";

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setExpirationDateInMs(Integer expirationDateInMs) {
        this.expirationDateInMs = expirationDateInMs;
    }

    public void setRestoreExpirationDateInMs(Integer restoreExpirationDateInMs) {
        this.restoreExpirationDateInMs = restoreExpirationDateInMs;
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();

        if (userDetails.getAuthorities()
                       .contains(JwtServiceConfig.AUTHORITY_USER)) {
            claims.put(JwtServiceConfig.IS_USER, true);
        }
        if (userDetails.getAuthorities()
                       .contains(JwtServiceConfig.AUTHORITY_ADMIN)) {
            claims.put(JwtServiceConfig.IS_ADMIN, true);
        }

        return generateToken(userDetails.getUsername(), claims);
    }

    private String generateToken(String subject, Map<String, Object> claims) {
        return Jwts.builder()
                   .setClaims(claims)
                   .setIssuedAt(new Date(System.currentTimeMillis()))
                   .setExpiration(new Date(System.currentTimeMillis() + expirationDateInMs))
                   .setSubject(subject)
                   .signWith(SignatureAlgorithm.HS512, secret).compact();

    }

    public String generateRestoreToken(String subject, Map<String, Object> claims) {
        return Jwts.builder()
                   .signWith(SignatureAlgorithm.HS512, secret)
                   .setClaims(claims)
                   .setIssuedAt(new Date(System.currentTimeMillis()))
                   .setExpiration(new Date(System.currentTimeMillis() + restoreExpirationDateInMs))
                   .setSubject(subject)
                   .compact();
    }

    public Boolean validateToken(String authToken) {
        try {
            return Jwts.parser()
                       .setSigningKey(secret)
                       .parseClaimsJws(authToken) != null;
        } catch (ExpiredJwtException ex) {
            throw ex;
        } catch (JwtException ex){
            throw new BadCredentialsException(JwtServiceConfig.INVALID_CREDENTIALS, ex);
        }
    }

    public String getUsernameFromToken(String token) {
        return Jwts.parser()
                   .setSigningKey(secret)
                   .parseClaimsJws(token)
                   .getBody()
                   .getSubject();
    }

    public List<SimpleGrantedAuthority> getRolesFromToken(String token) {
        var claims = Jwts.parser()
                         .setSigningKey(secret)
                         .parseClaimsJws(token)
                         .getBody();

        Boolean isAdmin = claims.get(JwtServiceConfig.IS_ADMIN, Boolean.class);
        Boolean isUser  = claims.get(JwtServiceConfig.IS_USER, Boolean.class);

        if (isUser != null && isUser) return Collections.singletonList(JwtServiceConfig.AUTHORITY_USER);

        return (isAdmin != null && isAdmin)
                ? Collections.singletonList(JwtServiceConfig.AUTHORITY_ADMIN)
                : null;
    }

}
