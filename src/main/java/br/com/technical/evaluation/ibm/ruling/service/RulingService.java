package br.com.technical.evaluation.ibm.ruling.service;

import br.com.technical.evaluation.ibm.ruling.dtos.RulingSaveDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingUpdateDto;
import br.com.technical.evaluation.ibm.ruling.exceptions.PlacedTimeIntervalIsExceededException;
import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;

import java.util.List;
import java.util.Optional;

public interface RulingService {

    Optional<RulingDto> save(RulingSaveDto rulingSaveDto) throws PlacedTimeIntervalIsExceededException;

    Optional<RulingDto> getById(Integer rulingId) throws RulingNotFoundException;

    List<RulingDto> listAll();

    Optional<RulingDto> update(RulingUpdateDto rulingUpdateDto) throws RulingNotFoundException;

    void remove(Integer rulingId) throws RulingNotFoundException;
}
