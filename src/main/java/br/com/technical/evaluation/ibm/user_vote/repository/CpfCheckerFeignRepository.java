package br.com.technical.evaluation.ibm.user_vote.repository;

import br.com.technical.evaluation.ibm.user_vote.dtos.CpfStatusDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cpf-checker", url = "${cpf-checker.url}")
public interface CpfCheckerFeignRepository {

    @GetMapping("/users/{userLoginCpf}")
    public CpfStatusDto checkUserCpfLogin(@PathVariable String userLoginCpf);
}
