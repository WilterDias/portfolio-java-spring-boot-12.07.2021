package br.com.technical.evaluation.ibm.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "exception.expiredTokenException")
public class ExpiredTokenException extends Exception {}
