package br.com.technical.evaluation.ibm.user_vote.controller;

import br.com.technical.evaluation.ibm.config.JwtRoleTypeEnum;
import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteForListDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteRemoveDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteSaveDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteUpdateDto;
import br.com.technical.evaluation.ibm.user_vote.exceptions.CpfDontHavePermissionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.ExpiredVotingSessionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserHasVotedException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserVoteNotFoundException;
import br.com.technical.evaluation.ibm.user_vote.service.UserVoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(value="${api.userVotes}")
@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/uservotes")
public class UserVoteController {
    private final UserVoteService userVoteService;

    @ApiOperation(value = "${api.userVotes.getByCpfLoginAndRulingId}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_USER, JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/byruling/{rulingId}")
    public Optional<UserVoteDto> getByCpfLoginAndRulingId(@AuthenticationPrincipal User user,
                                                          @PathVariable Integer rulingId)
            throws UserVoteNotFoundException {
        return this.userVoteService.getByCpfLoginAndRulingId(user.getUsername(), rulingId);
    }

    @ApiOperation(value = "${api.userVotes.listAllByRulingId}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @GetMapping("/list/byruling/{rulingId}")
    public List<UserVoteForListDto> listAllByRulingId(@PathVariable Integer rulingId) {
        return this.userVoteService.listAllByRulingId(rulingId);
    }

    @ApiOperation(value = "${api.userVotes.update}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @PutMapping(value = "/")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Optional<UserVoteUpdateDto> update(
            @AuthenticationPrincipal User user,
            @Valid @RequestBody UserVoteUpdateDto userVoteUpdateDto)
            throws UserVoteNotFoundException, CpfDontHavePermissionException {
        return this.userVoteService.update(user.getUsername(), userVoteUpdateDto);
    }

    @ApiOperation(value = "${api.userVotes.remove}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN})
    @DeleteMapping(value = "/byruling/{rulingId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@Valid @RequestBody UserVoteRemoveDto userVoteRemoveDto,
                       @PathVariable Integer rulingId) throws UserVoteNotFoundException {
        this.userVoteService.removeByCpfLoginAndRulingId(userVoteRemoveDto.getCpfLogin(), rulingId); // no content
    }

    @ApiOperation(value = "${api.userVotes.save}", authorizations = {@Authorization(value="JWT")})
    @Secured({JwtRoleTypeEnum.Constants.ROLE_ADMIN, JwtRoleTypeEnum.Constants.ROLE_USER})
    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public Optional<UserVoteDto> save(
                @AuthenticationPrincipal User user,
                @Valid @RequestBody UserVoteSaveDto userVoteSaveDto)
            throws ExpiredVotingSessionException,
                   UserHasVotedException,
                   RulingNotFoundException,
                   UserCpfLoginNotFoundException,
                   CpfDontHavePermissionException {
        return this.userVoteService.save(user.getUsername(), userVoteSaveDto);
    }
}
