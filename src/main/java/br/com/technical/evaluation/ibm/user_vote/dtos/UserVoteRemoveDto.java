package br.com.technical.evaluation.ibm.user_vote.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserVoteRemoveDto {
    @NotBlank(message = "{error.userVoteCpfLoginBlank}")
    private String cpfLogin;
}
