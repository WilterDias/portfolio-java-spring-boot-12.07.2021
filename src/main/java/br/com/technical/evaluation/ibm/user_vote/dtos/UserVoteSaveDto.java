package br.com.technical.evaluation.ibm.user_vote.dtos;

import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user_vote.model.UserVote;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserVoteSaveDto {
    @NotNull(message = "{error.userVoteRulingId}")
    private Integer rulingId;
    @NotNull(message = "{error.userVoteVoteYes}")
    private Boolean voteIsYes;

    public static UserVote userVoteSaveDtoToUserVote(Ruling ruling, UserJwt userJwt, UserVoteSaveDto userVoteSaveDto) {
        return UserVote.builder()
                       .identificator(null)
                       .userJwt(userJwt)
                       .ruling(ruling)
                       .voteIsYes(userVoteSaveDto.getVoteIsYes())
                       .build();
    }
}
