package br.com.technical.evaluation.ibm.user_vote.service;

import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;
import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import br.com.technical.evaluation.ibm.ruling.repository.RulingRepository;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user.repository.UserRepository;
import br.com.technical.evaluation.ibm.user_vote.dtos.CpfStatusDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.CpfStatusEnum;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteForListDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteSaveDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteUpdateDto;
import br.com.technical.evaluation.ibm.user_vote.exceptions.CpfDontHavePermissionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.ExpiredVotingSessionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserHasVotedException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserVoteNotFoundException;
import br.com.technical.evaluation.ibm.user_vote.model.UserVote;
import br.com.technical.evaluation.ibm.user_vote.repository.CpfCheckerFeignRepository;
import br.com.technical.evaluation.ibm.user_vote.repository.UserVoteRepository;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@SuppressWarnings({"java:S6212"})
public class UserVoteServiceImp implements UserVoteService{
    private final UserVoteRepository userVoteRepository;
    private final RulingRepository rulingRepository;
    private final UserRepository userJwtRepository;
    private final CpfCheckerFeignRepository cpfCheckerFeignRepository;

    @Override
    public Optional<UserVoteDto> save(String userCpfLogin, UserVoteSaveDto userVoteSaveDto)
            throws ExpiredVotingSessionException,
                   RulingNotFoundException,
                   UserCpfLoginNotFoundException,
                   UserHasVotedException,
                   CpfDontHavePermissionException {
        checkForVotingPermission(userCpfLogin);
        Optional<UserVote> userVoteOptional =
                userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(userCpfLogin,
                                                                                 userVoteSaveDto.getRulingId());
        if (userVoteOptional.isPresent()) {
            throw new UserHasVotedException();
        }
        Ruling ruling = rulingRepository.findById(userVoteSaveDto.getRulingId())
                                        .orElseThrow(RulingNotFoundException::new);
        UserJwt userJwt = userJwtRepository.findByCpfLogin(userCpfLogin)
                                           .orElseThrow(UserCpfLoginNotFoundException::new);
        verifyTimeSessionOfRuling(ruling.getSessionStartDate(), ruling.getSessionDurationInMinutes());
        UserVote userVote = UserVoteSaveDto.userVoteSaveDtoToUserVote(ruling, userJwt, userVoteSaveDto);
        return Optional.of(UserVoteDto.userVoteToUserVoteDto(userVoteRepository.save(userVote)));
    }

    @Override
    public Optional<UserVoteDto> getByCpfLoginAndRulingId(String userCpfLogin, Integer rulingId)
            throws UserVoteNotFoundException {
        UserVote userVote = userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(userCpfLogin, rulingId)
                                              .orElseThrow(UserVoteNotFoundException::new);
        return Optional.of(UserVoteDto.userVoteToUserVoteDto(userVote));
    }

    @Override
    public List<UserVoteForListDto> listAllByRulingId(Integer rulingId) {
        List<UserVote> listUserVote = userVoteRepository.findByRuling_Identificator(rulingId);
        return listUserVote.stream()
                           .map(UserVoteForListDto::userVoteToUserVoteForListDto)
                           .collect(Collectors.toList());
    }

    @Override
    public Optional<UserVoteUpdateDto> update(String userCpfLogin, UserVoteUpdateDto userVoteUpdateDto)
            throws UserVoteNotFoundException, CpfDontHavePermissionException {
        checkForVotingPermission(userCpfLogin);
        UserVote existing =
                userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(userVoteUpdateDto.getCpfLogin(),
                                                                                 userVoteUpdateDto.getRulingId())
                                  .orElseThrow(UserVoteNotFoundException::new);
        //Será possível alterar o voto depois da sessão ter sido expirada. No entanto deverá ter privilégio para isso.
        //E também permissão em relação ao voto. Não há verificação para a alteração da data e duração da sessão.
        UserVote userVote = UserVoteUpdateDto.userVoteUpdateDtoToUserVote(existing, userVoteUpdateDto);
        return Optional.of(UserVoteUpdateDto.userVoteToUserVoteUpdateDto(userVoteRepository.save(userVote)));
    }

    @Override
    public void removeByCpfLoginAndRulingId(String userCpfLogin, Integer rulingId) throws UserVoteNotFoundException {
        UserVote existing = userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(userCpfLogin, rulingId)
                                              .orElseThrow(UserVoteNotFoundException::new);
        userVoteRepository.delete(existing);
    }

    private void verifyTimeSessionOfRuling (LocalDateTime sessionStartDate, Integer duration)
            throws ExpiredVotingSessionException {
        if (LocalDate.now()
                     .isAfter(ChronoLocalDate.from(sessionStartDate.plus(duration,
                                                                         ChronoUnit.MINUTES)))) {
            throw new ExpiredVotingSessionException();
        }
    }

    private void checkForVotingPermission(String userCpfLogin) throws CpfDontHavePermissionException {
        CpfStatusDto check = null;
        try {
            check = cpfCheckerFeignRepository.checkUserCpfLogin(userCpfLogin);
        } catch (FeignException ignored) {
            //ignora exceção.
        }
        //Caso falhe a checagem permitirá o CPF, assumisse que não foi implementado o servidor de checagem de CPF.
        if (check != null && CpfStatusEnum.UNABLE_TO_VOTE.equals(check.getStatus())) {
            throw new CpfDontHavePermissionException();
        }
    }
}
