package br.com.technical.evaluation.ibm.user.service;

import br.com.technical.evaluation.ibm.user.dtos.UserDto;
import br.com.technical.evaluation.ibm.user.dtos.UserSaveDto;
import br.com.technical.evaluation.ibm.user.dtos.UserUpdateDto;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginExistException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {

    void save(UserSaveDto userSaveDto) throws UserCpfLoginExistException;

    Optional<UserDto> getByCpfLogin(String userCpfLogin) throws UserCpfLoginNotFoundException;

    List<UserDto> listAll();

    Optional<UserDto> update(UserUpdateDto userUpdateDto) throws UserCpfLoginNotFoundException;

    void remove(String userCpfLogin) throws UserCpfLoginNotFoundException;
}
