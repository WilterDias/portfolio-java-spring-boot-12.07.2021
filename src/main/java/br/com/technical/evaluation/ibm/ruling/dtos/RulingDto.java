package br.com.technical.evaluation.ibm.ruling.dtos;

import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RulingDto {
    @NotNull(message = "{error.rulingIdentificatorNull}")
    private Integer identificator;
    @NotBlank(message = "{error.rulingTitleBlank}")
    private String title;
    @NotBlank(message = "{error.rulingDescriptionBlank}")
    private String description;
    private String documentsUrl;
    @NotNull(message = "{error.rulingDateNull}")
    private LocalDateTime sessionStartDate;
    @Value("1")
    private Integer sessionDurationInMinutes;
    @Value("0")
    private Integer voteCountYes;
    @Value("0")
    private Integer voteCountTotal;
    //A quantidade de votos No é a quantidade total de votos subtraído pela quantidade de votos Yes (complementariedade).

    public static RulingDto rulingToRulingDto (Ruling ruling) {
        return RulingDto.builder()
                        .identificator(null)
                        .title(ruling.getTitle())
                        .description(ruling.getDescription())
                        .documentsUrl(ruling.getDocumentsUrl())
                        .sessionStartDate(ruling.getSessionStartDate())
                        .sessionDurationInMinutes(ruling.getSessionDurationInMinutes())
                        .voteCountYes(ruling.getUserVoteList()!=null? ruling.calculateVotesYes().intValue(): 0)
                        .voteCountTotal(ruling.getUserVoteList()!=null? ruling.getUserVoteList().size(): 0)
                        .build();
    }
}
