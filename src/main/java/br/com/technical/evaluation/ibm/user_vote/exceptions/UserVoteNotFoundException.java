package br.com.technical.evaluation.ibm.user_vote.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "exception.userVoteNotFoundException")
public class UserVoteNotFoundException extends Exception {}
