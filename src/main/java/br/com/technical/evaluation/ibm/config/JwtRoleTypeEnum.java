package br.com.technical.evaluation.ibm.config;

public enum JwtRoleTypeEnum {
    ROLE_USER(Constants.ROLE_USER),
    ROLE_ADMIN(Constants.ROLE_ADMIN);

    private static final JwtRoleTypeEnum[] values = JwtRoleTypeEnum.values();

    public static class Constants {
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
        public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";

        private Constants() {
            throw new IllegalStateException();
        }
    }

    private final String label;

    private JwtRoleTypeEnum(String label) {
        this.label = label;
    }

    public static JwtRoleTypeEnum getById(int id) {
        return values[id];
    }

    public String getLabel(){
        return this.label;
    }
}
