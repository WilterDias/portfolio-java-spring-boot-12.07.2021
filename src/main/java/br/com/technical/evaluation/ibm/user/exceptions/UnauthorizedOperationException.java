package br.com.technical.evaluation.ibm.user.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "exception.unauthorizedOperationException")
public class UnauthorizedOperationException extends Exception {}
