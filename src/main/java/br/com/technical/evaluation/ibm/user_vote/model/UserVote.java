package br.com.technical.evaluation.ibm.user_vote.model;

import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "userVote",
       uniqueConstraints = @UniqueConstraint(columnNames={"userJwtId", "rulingId"}))
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString(callSuper = true, includeFieldNames = true, doNotUseGetters = true)
public class UserVote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer identificator;
    @NotNull
    @OneToOne()
    @JoinColumn(name="userJwtId")
    private UserJwt userJwt;
    @NotNull
    @OneToOne()
    @JoinColumn(name="rulingId")
    @JsonIgnoreProperties({"userVoteList"})
    @ToString.Exclude
    private Ruling ruling;
    @NotNull
    private Boolean voteIsYes;
}
