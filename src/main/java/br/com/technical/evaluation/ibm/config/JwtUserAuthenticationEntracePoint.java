package br.com.technical.evaluation.ibm.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@Component
public class JwtUserAuthenticationEntracePoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {

        var exception =  (Exception) request.getAttribute("exception");

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream()
                .write(new ObjectMapper().writeValueAsBytes(writeOutputStream((exception != null),
                                                                              exception,
                                                                              authException)));
    }

    private Map<String, String> writeOutputStream(boolean hasException,
                                                  Exception exception,
                                                  AuthenticationException authException) {
        return hasException
                ? Collections.singletonMap("cause", exception.toString())
                : Collections.singletonMap("error", authExceptionMessage(authException));
    }

    private String authExceptionMessage(AuthenticationException authException) {
        return (authException.getCause() != null)
                    ? authException.getCause().toString() + " " + authException.getMessage()
                    : authException.getMessage();
    }

}
