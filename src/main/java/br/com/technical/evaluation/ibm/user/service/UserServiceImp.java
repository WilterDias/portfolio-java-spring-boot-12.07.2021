package br.com.technical.evaluation.ibm.user.service;

import br.com.technical.evaluation.ibm.user.dtos.UserDto;
import br.com.technical.evaluation.ibm.user.dtos.UserSaveDto;
import br.com.technical.evaluation.ibm.user.dtos.UserUpdateDto;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginExistException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder cryptographyEncoder;

    @Override
    public UserDetails loadUserByUsername(String cpfLogin) throws UsernameNotFoundException{
        UserJwt userJwt = userRepository.findByCpfLogin(cpfLogin)
                                        .orElseThrow(() ->
                                            new UsernameNotFoundException("exception.userCpfLoginNotFoundException"));
        return new User(userJwt.getCpfLogin(),
                        userJwt.getPassword(),
                        Collections.singletonList(new SimpleGrantedAuthority(userJwt.getRole())));
    }

    @Override
    public void save(UserSaveDto userSaveDto) throws UserCpfLoginExistException {
        if (userRepository.findByCpfLogin(userSaveDto.getCpfLogin()).isPresent()) {
            throw new UserCpfLoginExistException();
        }
        userSaveDto.setPassword(cryptographyEncoder.encode(userSaveDto.getPassword()));
        userRepository.save(UserSaveDto.userSaveDtoToUserJwt(null, userSaveDto));
    }

    @Override
    public Optional<UserDto> getByCpfLogin(String cpfLogin) throws UserCpfLoginNotFoundException {
        Optional<UserJwt> userJwt = userRepository.findByCpfLogin(cpfLogin);
        if (userJwt.isEmpty()) {
            throw new UserCpfLoginNotFoundException();
        }
        return Optional.of(UserDto.userJwtToUserDto(userJwt.get()));
    }

    @Override
    public List<UserDto> listAll() {
        List<UserJwt> listClient = userRepository.findAll();
        return listClient.stream()
                         .map(UserDto::userJwtToUserDto)
                         .collect(Collectors.toList());
    }

    @Override
    public Optional<UserDto> update(UserUpdateDto userUpdateDto) throws UserCpfLoginNotFoundException {
        UserJwt existing = userRepository.findByCpfLogin(userUpdateDto.getCpfLogin())
                                         .orElseThrow(UserCpfLoginNotFoundException::new);
        userUpdateDto.setPassword(cryptographyEncoder.encode(userUpdateDto.getPassword()));
        var userJwt = userRepository.save(UserUpdateDto.userUpdateDtoToUserJwt(existing.getIdentificator(),
                                                                                       userUpdateDto));
        return Optional.of(UserDto.userJwtToUserDto(userJwt));
    }

    @Override
    public void remove(String cpfLogin) throws UserCpfLoginNotFoundException {
        UserJwt existing = userRepository.findByCpfLogin(cpfLogin)
                                         .orElseThrow(UserCpfLoginNotFoundException::new);
        userRepository.deleteById(existing.getIdentificator());
    }

}
