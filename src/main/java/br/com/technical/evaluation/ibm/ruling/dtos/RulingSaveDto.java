package br.com.technical.evaluation.ibm.ruling.dtos;

import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Collections;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RulingSaveDto {
    @NotBlank(message = "{error.rulingTitleBlank}")
    private String title;
    @NotBlank(message = "{error.rulingDescriptionBlank}")
    private String description;
    private String documentsUrl;
    @NotNull(message = "{error.rulingDateNull}")
    private LocalDateTime sessionStartDate;
    @Value("1")
    private Integer sessionDurationInMinutes;

    public static Ruling rulingSaveDtoToRuling(RulingSaveDto rulingSaveDto) {
        return Ruling.builder()
                     .identificator(null)
                     .title(rulingSaveDto.getTitle())
                     .description(rulingSaveDto.getDescription())
                     .documentsUrl(rulingSaveDto.getDocumentsUrl())
                     .sessionStartDate(rulingSaveDto.getSessionStartDate())
                     .sessionDurationInMinutes(rulingSaveDto.getSessionDurationInMinutes())
                     .userVoteList(Collections.emptyList())
                     .build();
    }
}
