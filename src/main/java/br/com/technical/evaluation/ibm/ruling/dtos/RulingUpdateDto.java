package br.com.technical.evaluation.ibm.ruling.dtos;

import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RulingUpdateDto {
    @NotNull(message = "{error.rulingIdentificatorNull}")
    private Integer identificator;
    @NotBlank(message = "{error.rulingTitleBlank}")
    private String title;
    @NotBlank(message = "{error.rulingDescriptionBlank}")
    private String description;
    private String documentsUrl;
    @NotNull(message = "{error.rulingDateNull}")
    private LocalDateTime sessionStartDate;
    @Value("1")
    private Integer sessionDurationInMinutes;

    public static Ruling rulingUpdateDtoToRuling(Integer userIdentificator, RulingUpdateDto rulingUpdateDto) {
        return Ruling.builder()
                     .identificator(userIdentificator)
                     .title(rulingUpdateDto.getTitle())
                     .description(rulingUpdateDto.getDescription())
                     .documentsUrl(rulingUpdateDto.getDocumentsUrl())
                     .sessionStartDate(rulingUpdateDto.getSessionStartDate())
                     .sessionDurationInMinutes(rulingUpdateDto.getSessionDurationInMinutes())
                     .userVoteList(null)
                     .build();
    }
}
