package br.com.technical.evaluation.ibm.ruling.service;

import br.com.technical.evaluation.ibm.ruling.dtos.RulingDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingSaveDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingUpdateDto;
import br.com.technical.evaluation.ibm.ruling.exceptions.PlacedTimeIntervalIsExceededException;
import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;
import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import br.com.technical.evaluation.ibm.ruling.repository.RulingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RulingServiceImp implements RulingService{
    private final RulingRepository rulingRepository;

    @Override
    public Optional<RulingDto> save(RulingSaveDto rulingSaveDto) throws PlacedTimeIntervalIsExceededException {
        if (LocalDate.now()
                     .isAfter(ChronoLocalDate.from(rulingSaveDto.getSessionStartDate()
                                                                .plus(rulingSaveDto.getSessionDurationInMinutes(),
                                                                      ChronoUnit.MINUTES)))) {
        throw new PlacedTimeIntervalIsExceededException();
        }
        return Optional.of(
                RulingDto.rulingToRulingDto(rulingRepository.save(RulingSaveDto.rulingSaveDtoToRuling(rulingSaveDto)))
        );
    }

    @Override
    public Optional<RulingDto> getById(Integer rulingId) throws RulingNotFoundException {
        return Optional.of(RulingDto.rulingToRulingDto(rulingRepository.findById(rulingId)
                                                                       .orElseThrow(RulingNotFoundException::new)));
    }

    @Override
    public List<RulingDto> listAll() {
        List<Ruling> listRuling = rulingRepository.findAll();
        return listRuling.stream()
                         .map(RulingDto::rulingToRulingDto)
                         .collect(Collectors.toList());
    }

    @Override
    public Optional<RulingDto> update(RulingUpdateDto rulingUpdateDto) throws RulingNotFoundException {
        Ruling existing = rulingRepository.findById(rulingUpdateDto.getIdentificator())
                                          .orElseThrow(RulingNotFoundException::new);
        Ruling rulingUpdate = RulingUpdateDto.rulingUpdateDtoToRuling(existing.getIdentificator(), rulingUpdateDto);
        return Optional.of(RulingDto.rulingToRulingDto(rulingRepository.save(rulingUpdate)));
    }

    @Override
    public void remove(Integer rulingId) throws RulingNotFoundException {
        Ruling existing = rulingRepository.findById(rulingId)
                                          .orElseThrow(RulingNotFoundException::new);
        rulingRepository.deleteById(existing.getIdentificator());
    }
}
