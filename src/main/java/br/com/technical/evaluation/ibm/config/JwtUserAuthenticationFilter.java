package br.com.technical.evaluation.ibm.config;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ResourceBundle;

@Component
@Slf4j
public class JwtUserAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtServiceConfig jwtServiceConfig;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {

        var bundle = ResourceBundle.getBundle("i18n/messages");
        try{
            // O JWT Token deve estar no formato de "Bearer token". Remove-se a palavra Bearer para obter o Token.
            String jwtToken = getJwtFromRequest(request);

            if (StringUtils.hasText(jwtToken) && Boolean.TRUE.equals(jwtServiceConfig.validateToken(jwtToken))) {
                UserDetails userDetails = new User(jwtServiceConfig.getUsernameFromToken(jwtToken),
                                                   "",
                                                   jwtServiceConfig.getRolesFromToken(jwtToken));

                var usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(
                                userDetails,
                                null,
                                userDetails.getAuthorities());
                // Ao configurar o Authentication no contexto, será definido que o usuário atual é autenticado,
                // ele passará com sucesso pelas configuração do Spring Security.
                SecurityContextHolder.getContext()
                                     .setAuthentication(usernamePasswordAuthenticationToken);
            } else {
                var messageResponse = bundle.getString("authentication.filter.tokenJwtExpired");
                logger.warn(messageResponse);//TODO Pensar em uma maneira de lançar uma exceção com mensagem em português.
                var e = new Exception(bundle.getString("authentication.filter.warnSecurityContext"));
                request.setAttribute("message", e);
            }
        } catch (ExpiredJwtException ex) {
            String isRestoreToken = request.getHeader("isRestoreToken");
            var requestURL = request.getRequestURL().toString();
            // As condições a seguir se forem satisfeitas permitem a restauração do token.
            if ((isRestoreToken != null && isRestoreToken.equalsIgnoreCase("true")
                                        && requestURL.toLowerCase()
                                                     .contains("restoretoken"))) {
                allowForRestoreToken(ex, request);
            } else {
                var e = new ExpiredJwtException(
                        null,
                        null,
                        bundle.getString("authentication.filter.tokenJwtExpired"));
                request.setAttribute("exception", e);
            }
        } catch (BadCredentialsException ex) {
            var e = new BadCredentialsException(
                    bundle.getString("exception.invalidTokenCredentialsException"));
            request.setAttribute("exception", e);
        }
        chain.doFilter(request, response);
    }

    private void allowForRestoreToken(ExpiredJwtException ex, HttpServletRequest request) {

        var usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(null, null, null);
        // Ao configurar o Authentication no contexto, será definido que o usuário atual é autenticado,
        // ele passará com sucesso pelas configuração do Spring Security.
        SecurityContextHolder.getContext()
                             .setAuthentication(usernamePasswordAuthenticationToken);
        // Defini-se o claim para que possa criar um novo JWT no controlador
        request.setAttribute("claims", ex.getClaims());

    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        var bearer = "Bearer ";
        return (StringUtils.hasText(bearerToken) && bearerToken.startsWith(bearer))
                        ? bearerToken.substring(bearer.length(), bearerToken.length())
                        : null;
    }

}