package br.com.technical.evaluation.ibm.user.dtos;

import br.com.technical.evaluation.ibm.config.JwtRoleTypeEnum;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSaveDto {
    //@CPF(message = "{error.userCpfLogin}")//TODO Criar uma annotation para verificar o CPF.
    @NotBlank(message = "{error.userCpfLoginBlank}")
    private String cpfLogin;
    @NotBlank(message = "{error.userPasswordBlank}")
    private String password;
    @Email(message = "{error.userEmailFormat}")
    private String email;
    @Value(JwtRoleTypeEnum.Constants.ROLE_USER)
    @NotNull(message = "{error.userRoleTypeBlank}")
    private JwtRoleTypeEnum role;

    public static UserJwt userSaveDtoToUserJwt(Integer userIdentificator, UserSaveDto userSaveDto) {
        return UserJwt.builder()
                      .identificator(userIdentificator)
                      .cpfLogin(userSaveDto.getCpfLogin())
                      .password(userSaveDto.getPassword())
                      .email(userSaveDto.getEmail())
                      .role(userSaveDto.getRole()
                                         .getLabel())
                      .build();
    }
}
