package br.com.technical.evaluation.ibm.user.repository;

import br.com.technical.evaluation.ibm.user.model.UserJwt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserJwt, Integer> {
    Optional<UserJwt> findByCpfLogin(String cpfLogin);

    void deleteByCpfLogin(String cpfLogin);
}
