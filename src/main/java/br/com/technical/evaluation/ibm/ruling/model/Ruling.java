package br.com.technical.evaluation.ibm.ruling.model;

import br.com.technical.evaluation.ibm.user_vote.model.UserVote;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ruling")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString(callSuper=true, includeFieldNames=true, doNotUseGetters = true)
@SuppressWarnings({"java:S1948"})
public class Ruling implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer identificator;
    @NotBlank
    private String title;
    @NotBlank
    private String description;
    private String documentsUrl;
    @NotNull
    private LocalDateTime sessionStartDate;
    @NotNull
    private Integer sessionDurationInMinutes;
    @Builder.Default
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ruling")
    private List<UserVote> userVoteList = new ArrayList<>();

    //A quantidade de votos No é a quantidade total de votos subtraído pela quantidade de votos Yes (complementariedade).
    public Long calculateVotesYes() {
        if (this.userVoteList == null) {
            return 0L;
        }
        return this.userVoteList.stream()
                                .filter(it -> Boolean.TRUE
                                                     .equals(it.getVoteIsYes()))
                                .count();
    }
}
