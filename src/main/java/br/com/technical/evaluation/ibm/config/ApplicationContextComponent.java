package br.com.technical.evaluation.ibm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
@SuppressWarnings({"java:S2696","java:S3740"})
public class ApplicationContextComponent implements ApplicationContextAware {

    @Autowired
    private static ApplicationContext applicationContext;

    public void setApplicationContext(@NotNull final ApplicationContext applicationContext) {
        ApplicationContextComponent.applicationContext = applicationContext;
    }

    public static <T> T getBean(final Class cl) {
        return (T) applicationContext.getBean(cl);
    }

    public static ApplicationContext getContext() {
        return ApplicationContextComponent.applicationContext;
    }

}
