package br.com.technical.evaluation.ibm.user_vote.utils;

import br.com.technical.evaluation.ibm.user_vote.dtos.CpfStatusEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class EnumConverterUtils implements Converter<String, CpfStatusEnum> {

    @Override
    public CpfStatusEnum convert(String value) {
        return CpfStatusEnum.valueOf(value.toUpperCase());
    }
}
