package br.com.technical.evaluation.ibm.config;

import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration.FlywayConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class FlywayConfig extends FlywayConfiguration{
@Primary
@Bean
@DependsOn("applicationContextComponent")
public FlywayMigrationInitializer flywayInitializer(Flyway flyway){
    return super.flywayInitializer(flyway, null);
}

}
