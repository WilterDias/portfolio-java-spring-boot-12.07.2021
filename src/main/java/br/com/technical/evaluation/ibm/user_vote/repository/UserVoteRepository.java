package br.com.technical.evaluation.ibm.user_vote.repository;

import br.com.technical.evaluation.ibm.user_vote.model.UserVote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@SuppressWarnings({"java:S100"})
public interface UserVoteRepository extends JpaRepository<UserVote, Integer> {

    List<UserVote> findByRuling_Identificator(Integer rulingId);

    void deleteByUserJwt_CpfLoginAndRuling_Identificator(String userCpfLogin, Integer rulingId);

    Optional<UserVote> findByUserJwt_CpfLoginAndRuling_Identificator(String userCpfLogin, Integer rulingId);
}
