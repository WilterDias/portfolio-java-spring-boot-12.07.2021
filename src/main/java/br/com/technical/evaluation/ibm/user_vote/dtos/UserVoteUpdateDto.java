package br.com.technical.evaluation.ibm.user_vote.dtos;

import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user_vote.model.UserVote;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserVoteUpdateDto {
    //@CPF(message = "{error.userCpfLogin}")//TODO Criar uma annotation para verificar o CPF.
    @NotBlank(message = "{error.userVoteCpfLoginBlank}")
    private String cpfLogin;
    @NotNull(message = "{error.userVoteRulingId}")
    private Integer rulingId;
    @NotNull(message = "{error.userVoteVoteYes}")
    private Boolean voteIsYes;

    public static UserVote userVoteUpdateDtoToUserVote(UserVote existing, UserVoteUpdateDto userVoteUpdateDto) {
        return UserVote.builder()
                       .identificator(existing.getIdentificator())
                       .userJwt(existing.getUserJwt())
                       .ruling(existing.getRuling())
                       .voteIsYes(userVoteUpdateDto.getVoteIsYes())
                       .build();
    }

    public static UserVoteUpdateDto userVoteToUserVoteUpdateDto(UserVote userVote) {
        return UserVoteUpdateDto.builder()
                .cpfLogin(userVote.getUserJwt().getCpfLogin())
                .rulingId(userVote.getRuling().getIdentificator())
                .voteIsYes(userVote.getVoteIsYes())
                .build();
    }
}
