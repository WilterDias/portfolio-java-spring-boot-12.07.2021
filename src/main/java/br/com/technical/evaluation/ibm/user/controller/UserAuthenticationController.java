package br.com.technical.evaluation.ibm.user.controller;

import br.com.technical.evaluation.ibm.config.JwtServiceConfig;
import br.com.technical.evaluation.ibm.user.dtos.UserAuthenticationRequestDto;
import br.com.technical.evaluation.ibm.user.dtos.UserAuthenticationResponseDto;
import br.com.technical.evaluation.ibm.user.exceptions.InvalidTokenCredentialsException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user.exceptions.UserDisabledException;
import br.com.technical.evaluation.ibm.user.service.UserService;
import io.jsonwebtoken.impl.DefaultClaims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Api(value="${api.users}")
@RestController
@RequiredArgsConstructor
@RequestMapping(value="/api/users")
public class UserAuthenticationController {
    
    private final AuthenticationManager authenticationManager;
    
    private final UserService userService;

    private final JwtServiceConfig jwtServiceConfig;

    @ApiOperation(value = "${api.users.generateToken}")
    @PostMapping(value = "/authenticate")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public UserAuthenticationResponseDto generateToken(@RequestBody UserAuthenticationRequestDto userAuthenticationRequestDto)
            throws UserDisabledException, InvalidTokenCredentialsException, UserCpfLoginNotFoundException {
        try {
            this.authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userAuthenticationRequestDto.getCpfLogin(),
                            userAuthenticationRequestDto.getPassword()));

            UserDetails userDetails = this.userService.loadUserByUsername(userAuthenticationRequestDto.getCpfLogin());
            return new UserAuthenticationResponseDto(this.jwtServiceConfig.generateToken(userDetails));
        } catch (DisabledException e) {
            throw new UserDisabledException();
        } catch (BadCredentialsException e) {
            throw new InvalidTokenCredentialsException();
        } catch (UsernameNotFoundException e) {
            throw new UserCpfLoginNotFoundException();
        }

    }

    @ApiOperation(value = "${api.users.restoretoken}",
                  notes ="${api.users.restoretoken.note}",
                  authorizations = {@Authorization(value="JWT")})
    @GetMapping("/restoreToken") //requer uma flag chamada isRestoreToken como true no header.
    public UserAuthenticationResponseDto restoretoken(HttpServletRequest request) {
        Map<String, Object> claimMap = new HashMap<>();
        ((DefaultClaims) request.getAttribute("claims"))
                                .forEach(claimMap::put);
        String token = this.jwtServiceConfig.generateRestoreToken(claimMap.get("sub")
                                                                          .toString(), claimMap);
        return new UserAuthenticationResponseDto(token);
    }

}

