package br.com.technical.evaluation.ibm.ruling.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "exception.rulingNotFoundException")
public class RulingNotFoundException extends Exception {}
