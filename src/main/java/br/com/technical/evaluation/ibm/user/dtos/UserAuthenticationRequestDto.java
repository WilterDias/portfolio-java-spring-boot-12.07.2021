package br.com.technical.evaluation.ibm.user.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthenticationRequestDto {
    @NotBlank(message = "{error.userCpfLoginBlank}")
    private String cpfLogin;
    @NotBlank(message = "{error.userPasswordBlank}")
    private String password;

}
