package br.com.technical.evaluation.ibm.ruling.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.technical.evaluation.ibm.ruling.dtos.RulingDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingSaveDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingUpdateDto;
import br.com.technical.evaluation.ibm.ruling.exceptions.PlacedTimeIntervalIsExceededException;
import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;
import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import br.com.technical.evaluation.ibm.ruling.repository.RulingRepository;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user_vote.model.UserVote;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {RulingServiceImp.class})
@ExtendWith(SpringExtension.class)
class RulingServiceImpTest {
    @MockBean
    private RulingRepository rulingRepository;

    @Autowired
    private RulingServiceImp rulingServiceImp;

    @Test
    void testSave() throws PlacedTimeIntervalIsExceededException {
        RulingSaveDto rulingSaveDto = new RulingSaveDto("Dr", "The characteristics of someone or something",
                "https://example.org/example", LocalDateTime.of(1, 1, 1, 1, 1), 1);
        rulingSaveDto.setSessionDurationInMinutes(1);
        assertThrows(PlacedTimeIntervalIsExceededException.class, () -> this.rulingServiceImp.save(rulingSaveDto));
    }

    @Test
    void testGetById() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        LocalDateTime ofResult = LocalDateTime.of(1, 1, 1, 1, 1);
        ruling.setSessionStartDate(ofResult);
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());
        Optional<Ruling> ofResult1 = Optional.<Ruling>of(ruling);
        when(this.rulingRepository.findById((Integer) any())).thenReturn(ofResult1);
        Optional<RulingDto> actualById = this.rulingServiceImp.getById(123);
        assertTrue(actualById.isPresent());
        RulingDto getResult = actualById.get();
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertEquals(0, getResult.getVoteCountYes().intValue());
        assertEquals(0, getResult.getVoteCountTotal().intValue());
        assertEquals("Dr", getResult.getTitle());
        assertSame(ofResult, getResult.getSessionStartDate());
        assertEquals(1, getResult.getSessionDurationInMinutes().intValue());
        assertEquals("https://example.org/example", getResult.getDocumentsUrl());
        assertNull(getResult.getIdentificator());
        verify(this.rulingRepository).findById((Integer) any());
    }

    @Test
    void testGetById2() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole("Role");
        userJwt.setCpfLogin("Cpf Login");

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        LocalDateTime ofResult = LocalDateTime.of(1, 1, 1, 1, 1);
        ruling1.setSessionStartDate(ofResult);
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(1);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(userVoteList);
        Optional<Ruling> ofResult1 = Optional.<Ruling>of(ruling1);
        when(this.rulingRepository.findById((Integer) any())).thenReturn(ofResult1);
        Optional<RulingDto> actualById = this.rulingServiceImp.getById(123);
        assertTrue(actualById.isPresent());
        RulingDto getResult = actualById.get();
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertEquals(1, getResult.getVoteCountYes().intValue());
        assertEquals(1, getResult.getVoteCountTotal().intValue());
        assertEquals("Dr", getResult.getTitle());
        assertSame(ofResult, getResult.getSessionStartDate());
        assertEquals(1, getResult.getSessionDurationInMinutes().intValue());
        assertEquals("https://example.org/example", getResult.getDocumentsUrl());
        assertNull(getResult.getIdentificator());
        verify(this.rulingRepository).findById((Integer) any());
    }

    @Test
    void testGetById3() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole("Role");
        userJwt.setCpfLogin("Cpf Login");

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(0);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt1 = new UserJwt();
        userJwt1.setIdentificator(1);
        userJwt1.setPassword("iloveyou");
        userJwt1.setEmail("jane.doe@example.org");
        userJwt1.setRole("Role");
        userJwt1.setCpfLogin("Cpf Login");

        UserVote userVote1 = new UserVote();
        userVote1.setIdentificator(1);
        userVote1.setRuling(ruling1);
        userVote1.setVoteIsYes(true);
        userVote1.setUserJwt(userJwt1);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote1);
        userVoteList.add(userVote);

        Ruling ruling2 = new Ruling();
        ruling2.setDocumentsUrl("https://example.org/example");
        ruling2.setIdentificator(1);
        LocalDateTime ofResult = LocalDateTime.of(1, 1, 1, 1, 1);
        ruling2.setSessionStartDate(ofResult);
        ruling2.setTitle("Dr");
        ruling2.setSessionDurationInMinutes(1);
        ruling2.setDescription("The characteristics of someone or something");
        ruling2.setUserVoteList(userVoteList);
        Optional<Ruling> ofResult1 = Optional.<Ruling>of(ruling2);
        when(this.rulingRepository.findById((Integer) any())).thenReturn(ofResult1);
        Optional<RulingDto> actualById = this.rulingServiceImp.getById(123);
        assertTrue(actualById.isPresent());
        RulingDto getResult = actualById.get();
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertEquals(2, getResult.getVoteCountYes().intValue());
        assertEquals(2, getResult.getVoteCountTotal().intValue());
        assertEquals("Dr", getResult.getTitle());
        assertSame(ofResult, getResult.getSessionStartDate());
        assertEquals(1, getResult.getSessionDurationInMinutes().intValue());
        assertEquals("https://example.org/example", getResult.getDocumentsUrl());
        assertNull(getResult.getIdentificator());
        verify(this.rulingRepository).findById((Integer) any());
    }

    @Test
    void testGetById4() throws RulingNotFoundException {
        when(this.rulingRepository.findById((Integer) any())).thenReturn(Optional.<Ruling>empty());
        Assertions.assertThrows(RulingNotFoundException.class, () ->
                this.rulingServiceImp.getById(123));
        verify(this.rulingRepository).findById((Integer) any());
    }

    @Test
    void testListAll() {
        when(this.rulingRepository.findAll()).thenReturn(new ArrayList<Ruling>());
        assertTrue(this.rulingServiceImp.listAll().isEmpty());
        verify(this.rulingRepository).findAll();
    }

    @Test
    void testListAll2() {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        ArrayList<Ruling> rulingList = new ArrayList<Ruling>();
        rulingList.add(ruling);
        when(this.rulingRepository.findAll()).thenReturn(rulingList);
        assertEquals(1, this.rulingServiceImp.listAll().size());
        verify(this.rulingRepository).findAll();
    }

    @Test
    void testListAll3() {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(0);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());

        ArrayList<Ruling> rulingList = new ArrayList<Ruling>();
        rulingList.add(ruling1);
        rulingList.add(ruling);
        when(this.rulingRepository.findAll()).thenReturn(rulingList);
        assertEquals(2, this.rulingServiceImp.listAll().size());
        verify(this.rulingRepository).findAll();
    }

    @Test
    void testListAll4() {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole("Role");
        userJwt.setCpfLogin("Cpf Login");

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(0);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(userVoteList);

        ArrayList<Ruling> rulingList = new ArrayList<Ruling>();
        rulingList.add(ruling1);
        when(this.rulingRepository.findAll()).thenReturn(rulingList);
        assertEquals(1, this.rulingServiceImp.listAll().size());
        verify(this.rulingRepository).findAll();
    }

    @Test
    void testListAll5() {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole("Role");
        userJwt.setCpfLogin("Cpf Login");

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(0);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt1 = new UserJwt();
        userJwt1.setIdentificator(1);
        userJwt1.setPassword("iloveyou");
        userJwt1.setEmail("jane.doe@example.org");
        userJwt1.setRole("Role");
        userJwt1.setCpfLogin("Cpf Login");

        UserVote userVote1 = new UserVote();
        userVote1.setIdentificator(1);
        userVote1.setRuling(ruling1);
        userVote1.setVoteIsYes(true);
        userVote1.setUserJwt(userJwt1);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote1);
        userVoteList.add(userVote);

        Ruling ruling2 = new Ruling();
        ruling2.setDocumentsUrl("https://example.org/example");
        ruling2.setIdentificator(1);
        ruling2.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling2.setTitle("Dr");
        ruling2.setSessionDurationInMinutes(0);
        ruling2.setDescription("The characteristics of someone or something");
        ruling2.setUserVoteList(userVoteList);

        ArrayList<Ruling> rulingList = new ArrayList<Ruling>();
        rulingList.add(ruling2);
        when(this.rulingRepository.findAll()).thenReturn(rulingList);
        assertEquals(1, this.rulingServiceImp.listAll().size());
        verify(this.rulingRepository).findAll();
    }

    @Test
    void testUpdate() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());
        Optional<Ruling> ofResult = Optional.<Ruling>of(ruling);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        LocalDateTime ofResult1 = LocalDateTime.of(1, 1, 1, 1, 1);
        ruling1.setSessionStartDate(ofResult1);
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(1);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());
        when(this.rulingRepository.save((Ruling) any())).thenReturn(ruling1);
        when(this.rulingRepository.findById((Integer) any())).thenReturn(ofResult);
        Optional<RulingDto> actualUpdateResult = this.rulingServiceImp.update(new RulingUpdateDto());
        assertTrue(actualUpdateResult.isPresent());
        RulingDto getResult = actualUpdateResult.get();
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertEquals(0, getResult.getVoteCountYes().intValue());
        assertEquals(0, getResult.getVoteCountTotal().intValue());
        assertEquals("Dr", getResult.getTitle());
        assertSame(ofResult1, getResult.getSessionStartDate());
        assertEquals(1, getResult.getSessionDurationInMinutes().intValue());
        assertEquals("https://example.org/example", getResult.getDocumentsUrl());
        assertNull(getResult.getIdentificator());
        verify(this.rulingRepository).findById((Integer) any());
        verify(this.rulingRepository).save((Ruling) any());
    }

    @Test
    void testUpdate2() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());
        Optional<Ruling> ofResult = Optional.<Ruling>of(ruling);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(0);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole("Role");
        userJwt.setCpfLogin("Cpf Login");

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling1);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote);

        Ruling ruling2 = new Ruling();
        ruling2.setDocumentsUrl("https://example.org/example");
        ruling2.setIdentificator(1);
        LocalDateTime ofResult1 = LocalDateTime.of(1, 1, 1, 1, 1);
        ruling2.setSessionStartDate(ofResult1);
        ruling2.setTitle("Dr");
        ruling2.setSessionDurationInMinutes(1);
        ruling2.setDescription("The characteristics of someone or something");
        ruling2.setUserVoteList(userVoteList);
        when(this.rulingRepository.save((Ruling) any())).thenReturn(ruling2);
        when(this.rulingRepository.findById((Integer) any())).thenReturn(ofResult);
        Optional<RulingDto> actualUpdateResult = this.rulingServiceImp.update(new RulingUpdateDto());
        assertTrue(actualUpdateResult.isPresent());
        RulingDto getResult = actualUpdateResult.get();
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertEquals(1, getResult.getVoteCountYes().intValue());
        assertEquals(1, getResult.getVoteCountTotal().intValue());
        assertEquals("Dr", getResult.getTitle());
        assertSame(ofResult1, getResult.getSessionStartDate());
        assertEquals(1, getResult.getSessionDurationInMinutes().intValue());
        assertEquals("https://example.org/example", getResult.getDocumentsUrl());
        assertNull(getResult.getIdentificator());
        verify(this.rulingRepository).findById((Integer) any());
        verify(this.rulingRepository).save((Ruling) any());
    }

    @Test
    void testUpdate3() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());
        Optional<Ruling> ofResult = Optional.<Ruling>of(ruling);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(0);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole("Role");
        userJwt.setCpfLogin("Cpf Login");

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling1);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        Ruling ruling2 = new Ruling();
        ruling2.setDocumentsUrl("https://example.org/example");
        ruling2.setIdentificator(1);
        ruling2.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling2.setTitle("Dr");
        ruling2.setSessionDurationInMinutes(0);
        ruling2.setDescription("The characteristics of someone or something");
        ruling2.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt1 = new UserJwt();
        userJwt1.setIdentificator(1);
        userJwt1.setPassword("iloveyou");
        userJwt1.setEmail("jane.doe@example.org");
        userJwt1.setRole("Role");
        userJwt1.setCpfLogin("Cpf Login");

        UserVote userVote1 = new UserVote();
        userVote1.setIdentificator(1);
        userVote1.setRuling(ruling2);
        userVote1.setVoteIsYes(true);
        userVote1.setUserJwt(userJwt1);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote1);
        userVoteList.add(userVote);

        Ruling ruling3 = new Ruling();
        ruling3.setDocumentsUrl("https://example.org/example");
        ruling3.setIdentificator(1);
        LocalDateTime ofResult1 = LocalDateTime.of(1, 1, 1, 1, 1);
        ruling3.setSessionStartDate(ofResult1);
        ruling3.setTitle("Dr");
        ruling3.setSessionDurationInMinutes(1);
        ruling3.setDescription("The characteristics of someone or something");
        ruling3.setUserVoteList(userVoteList);
        when(this.rulingRepository.save((Ruling) any())).thenReturn(ruling3);
        when(this.rulingRepository.findById((Integer) any())).thenReturn(ofResult);
        Optional<RulingDto> actualUpdateResult = this.rulingServiceImp.update(new RulingUpdateDto());
        assertTrue(actualUpdateResult.isPresent());
        RulingDto getResult = actualUpdateResult.get();
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertEquals(2, getResult.getVoteCountYes().intValue());
        assertEquals(2, getResult.getVoteCountTotal().intValue());
        assertEquals("Dr", getResult.getTitle());
        assertSame(ofResult1, getResult.getSessionStartDate());
        assertEquals(1, getResult.getSessionDurationInMinutes().intValue());
        assertEquals("https://example.org/example", getResult.getDocumentsUrl());
        assertNull(getResult.getIdentificator());
        verify(this.rulingRepository).findById((Integer) any());
        verify(this.rulingRepository).save((Ruling) any());
    }

    @Test
    void testUpdate4() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(2021, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());
        when(this.rulingRepository.save((Ruling) any())).thenReturn(ruling);
        when(this.rulingRepository.findById((Integer) any())).thenReturn(Optional.<Ruling>empty());
        Assertions.assertThrows(RulingNotFoundException.class, () ->
                this.rulingServiceImp.update(new RulingUpdateDto()));
        verify(this.rulingRepository).findById((Integer) any());
    }

    @Test
    void testRemove() throws RulingNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(2021, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());
        Optional<Ruling> ofResult = Optional.<Ruling>of(ruling);
        doNothing().when(this.rulingRepository).deleteById((Integer) any());
        when(this.rulingRepository.findById((Integer) any())).thenReturn(ofResult);
        this.rulingServiceImp.remove(123);
        verify(this.rulingRepository).deleteById((Integer) any());
        verify(this.rulingRepository).findById((Integer) any());
    }

    @Test
    void testRemove2() throws RulingNotFoundException {
        doNothing().when(this.rulingRepository).deleteById((Integer) any());
        when(this.rulingRepository.findById((Integer) any())).thenReturn(Optional.<Ruling>empty());
        Assertions.assertThrows(RulingNotFoundException.class, () ->
                this.rulingServiceImp.remove(123));
        verify(this.rulingRepository).findById((Integer) any());
    }
}

