package br.com.technical.evaluation.ibm.user_vote.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.technical.evaluation.ibm.ruling.exceptions.RulingNotFoundException;
import br.com.technical.evaluation.ibm.ruling.model.Ruling;
import br.com.technical.evaluation.ibm.ruling.repository.RulingRepository;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user.repository.UserRepository;
import br.com.technical.evaluation.ibm.user_vote.dtos.CpfStatusDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.CpfStatusEnum;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteForListDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteSaveDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteUpdateDto;
import br.com.technical.evaluation.ibm.user_vote.exceptions.CpfDontHavePermissionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.ExpiredVotingSessionException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserHasVotedException;
import br.com.technical.evaluation.ibm.user_vote.exceptions.UserVoteNotFoundException;
import br.com.technical.evaluation.ibm.user_vote.model.UserVote;
import br.com.technical.evaluation.ibm.user_vote.repository.CpfCheckerFeignRepository;
import br.com.technical.evaluation.ibm.user_vote.repository.UserVoteRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserVoteServiceImp.class})
@ExtendWith(SpringExtension.class)
class UserVoteServiceImpTest {
    @MockBean
    private CpfCheckerFeignRepository cpfCheckerFeignRepository;

    @MockBean
    private RulingRepository rulingRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserVoteRepository userVoteRepository;

    @Autowired
    private UserVoteServiceImp userVoteServiceImp;
    
    private final String CPF = "22174278065";
    private final String ROLE_USER = "ROLE_USER";
    
    @Test
    void testSave()
            throws RulingNotFoundException,
            UserCpfLoginNotFoundException,
            CpfDontHavePermissionException,
            ExpiredVotingSessionException,
            UserHasVotedException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);
        Optional<UserVote> ofResult = Optional.<UserVote>of(userVote);
        when(this.userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any()))
                .thenReturn(ofResult);
        when(this.cpfCheckerFeignRepository.checkUserCpfLogin(anyString()))
                .thenReturn(new CpfStatusDto(CpfStatusEnum.ABLE_TO_VOTE));
        assertThrows(UserHasVotedException.class,
                () -> this.userVoteServiceImp.save(CPF, new UserVoteSaveDto()));
        verify(this.userVoteRepository).findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
        verify(this.cpfCheckerFeignRepository).checkUserCpfLogin(anyString());
    }

    @Test
    void testGetByCpfLoginAndRulingId() throws UserVoteNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);
        Optional<UserVote> ofResult = Optional.<UserVote>of(userVote);
        when(this.userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any()))
                .thenReturn(ofResult);
        Optional<UserVoteDto> actualByCpfLoginAndRulingId = this.userVoteServiceImp
                .getByCpfLoginAndRulingId(CPF, 123);
        assertTrue(actualByCpfLoginAndRulingId.isPresent());
        UserVoteDto getResult = actualByCpfLoginAndRulingId.get();
        assertEquals(1, getResult.getRulingId().intValue());
        assertTrue(getResult.getVoteIsYes());
        verify(this.userVoteRepository).findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
    }

    @Test
    void testGetByCpfLoginAndRulingId2() throws UserVoteNotFoundException {
        when(this.userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any()))
                .thenReturn(Optional.<UserVote>empty());
        Assertions.assertThrows(UserVoteNotFoundException.class, () ->
                this.userVoteServiceImp.getByCpfLoginAndRulingId(CPF, 123));
        verify(this.userVoteRepository).findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
    }

    @Test
    void testListAllByRulingId() {
        when(this.userVoteRepository.findByRuling_Identificator((Integer) any())).thenReturn(new ArrayList<UserVote>());
        assertTrue(this.userVoteServiceImp.listAllByRulingId(123).isEmpty());
        verify(this.userVoteRepository).findByRuling_Identificator((Integer) any());
    }

    @Test
    void testListAllByRulingId2() {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote);
        when(this.userVoteRepository.findByRuling_Identificator((Integer) any())).thenReturn(userVoteList);
        List<UserVoteForListDto> actualListAllByRulingIdResult = this.userVoteServiceImp.listAllByRulingId(123);
        assertEquals(1, actualListAllByRulingIdResult.size());
        UserVoteForListDto getResult = actualListAllByRulingIdResult.get(0);
        assertNull(getResult.getCpfLogin());
        assertTrue(getResult.getVoteIsYes());
        assertEquals(1, getResult.getRulingId().intValue());
        verify(this.userVoteRepository).findByRuling_Identificator((Integer) any());
    }

    @Test
    void testListAllByRulingId3() {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(0);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(0);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt1 = new UserJwt();
        userJwt1.setIdentificator(1);
        userJwt1.setPassword("iloveyou");
        userJwt1.setEmail("jane.doe@example.org");
        userJwt1.setRole(ROLE_USER);
        userJwt1.setCpfLogin(CPF);

        UserVote userVote1 = new UserVote();
        userVote1.setIdentificator(1);
        userVote1.setRuling(ruling1);
        userVote1.setVoteIsYes(true);
        userVote1.setUserJwt(userJwt1);

        ArrayList<UserVote> userVoteList = new ArrayList<UserVote>();
        userVoteList.add(userVote1);
        userVoteList.add(userVote);
        when(this.userVoteRepository.findByRuling_Identificator((Integer) any())).thenReturn(userVoteList);
        List<UserVoteForListDto> actualListAllByRulingIdResult = this.userVoteServiceImp.listAllByRulingId(123);
        assertEquals(2, actualListAllByRulingIdResult.size());
        UserVoteForListDto getResult = actualListAllByRulingIdResult.get(0);
        assertTrue(getResult.getVoteIsYes());
        UserVoteForListDto getResult1 = actualListAllByRulingIdResult.get(1);
        assertTrue(getResult1.getVoteIsYes());
        assertEquals(1, getResult1.getRulingId().intValue());
        assertNull(getResult1.getCpfLogin());
        assertEquals(1, getResult.getRulingId().intValue());
        assertNull(getResult.getCpfLogin());
        verify(this.userVoteRepository).findByRuling_Identificator((Integer) any());
    }

    @Test
    void testUpdate() throws CpfDontHavePermissionException, UserVoteNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);
        Optional<UserVote> ofResult = Optional.<UserVote>of(userVote);

        Ruling ruling1 = new Ruling();
        ruling1.setDocumentsUrl("https://example.org/example");
        ruling1.setIdentificator(1);
        ruling1.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling1.setTitle("Dr");
        ruling1.setSessionDurationInMinutes(1);
        ruling1.setDescription("The characteristics of someone or something");
        ruling1.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt1 = new UserJwt();
        userJwt1.setIdentificator(1);
        userJwt1.setPassword("iloveyou");
        userJwt1.setEmail("jane.doe@example.org");
        userJwt1.setRole(ROLE_USER);
        userJwt1.setCpfLogin(CPF);

        UserVote userVote1 = new UserVote();
        userVote1.setIdentificator(1);
        userVote1.setRuling(ruling1);
        userVote1.setVoteIsYes(true);
        userVote1.setUserJwt(userJwt1);
        when(this.userVoteRepository.save((UserVote) any())).thenReturn(userVote1);
        when(this.userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any()))
                .thenReturn(ofResult);
        when(this.cpfCheckerFeignRepository.checkUserCpfLogin(anyString()))
                .thenReturn(new CpfStatusDto(CpfStatusEnum.ABLE_TO_VOTE));
        Optional<UserVoteUpdateDto> actualUpdateResult = this.userVoteServiceImp.update(CPF,
                UserVoteUpdateDto.builder().cpfLogin(CPF).build());
        assertTrue(actualUpdateResult.isPresent());
        UserVoteUpdateDto getResult = actualUpdateResult.get();
        assertEquals(CPF, getResult.getCpfLogin());
        assertTrue(getResult.getVoteIsYes());
        assertEquals(1, getResult.getRulingId().intValue());
        verify(this.userVoteRepository).findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
        verify(this.userVoteRepository).save((UserVote) any());
        verify(this.cpfCheckerFeignRepository).checkUserCpfLogin(anyString());
    }

    @Test
    void testRemoveByCpfLoginAndRulingId() throws UserVoteNotFoundException {
        Ruling ruling = new Ruling();
        ruling.setDocumentsUrl("https://example.org/example");
        ruling.setIdentificator(1);
        ruling.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        ruling.setTitle("Dr");
        ruling.setSessionDurationInMinutes(1);
        ruling.setDescription("The characteristics of someone or something");
        ruling.setUserVoteList(new ArrayList<UserVote>());

        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);

        UserVote userVote = new UserVote();
        userVote.setIdentificator(1);
        userVote.setRuling(ruling);
        userVote.setVoteIsYes(true);
        userVote.setUserJwt(userJwt);
        Optional<UserVote> ofResult = Optional.<UserVote>of(userVote);
        doNothing().when(this.userVoteRepository)
                .deleteByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
        when(this.userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any()))
                .thenReturn(ofResult);
        this.userVoteServiceImp.removeByCpfLoginAndRulingId(CPF, 123);
        verify(this.userVoteRepository).delete((UserVote) any());
        verify(this.userVoteRepository).findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
    }

    @Test
    void testRemoveByCpfLoginAndRulingId2() throws UserVoteNotFoundException {
        doNothing().when(this.userVoteRepository)
                .deleteByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
        when(this.userVoteRepository.findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any()))
                .thenReturn(Optional.<UserVote>empty());
        Assertions.assertThrows(UserVoteNotFoundException.class, () ->
                this.userVoteServiceImp.removeByCpfLoginAndRulingId(CPF, 123));
        verify(this.userVoteRepository).findByUserJwt_CpfLoginAndRuling_Identificator(anyString(), (Integer) any());
    }
}

