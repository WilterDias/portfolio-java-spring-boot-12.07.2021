package br.com.technical.evaluation.ibm.user.controller;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.technical.evaluation.ibm.config.JwtRoleTypeEnum;
import br.com.technical.evaluation.ibm.user.dtos.UserDto;
import br.com.technical.evaluation.ibm.user.exceptions.UnauthorizedOperationException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginExistException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user.repository.UserRepository;
import br.com.technical.evaluation.ibm.user.service.UserService;
import br.com.technical.evaluation.ibm.user.service.UserServiceImp;

import java.util.ArrayList;

import java.util.Optional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {UserController.class})
@ExtendWith(SpringExtension.class)
class UserControllerTest {
    @Autowired
    private UserController userController;

    @MockBean
    private UserService userService;

    @Test
    void testGetByCpfLogin() throws Exception {
        when(this.userService.getByCpfLogin(anyString())).thenReturn(
                Optional.<UserDto>of(new UserDto("Cpf Login", "jane.doe@example.org", JwtRoleTypeEnum.ROLE_USER)));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/users/{userCpfLogin}",
                "User Cpf Login");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"cpfLogin\":\"Cpf Login\"," +
                                        "\"email\":\"jane.doe@example.org\",\"role\":\"ROLE_USER\"}"));
    }

    @Test
    void testListAll() throws Exception {
        when(this.userService.listAll()).thenReturn(new ArrayList<UserDto>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/users/list");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Test
    void testListAll2() throws Exception {
        when(this.userService.listAll()).thenReturn(new ArrayList<UserDto>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/users/list");
        getResult.contentType("Not all who wander are lost");
        MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Disabled("Não foi possível fazer o teste da forma correta.")
    @Test
    void testUpdate() throws UnauthorizedOperationException, UserCpfLoginNotFoundException {
//        UserRepository userRepository = mock(UserRepository.class);
//        UserController userController = new UserController(new UserServiceImp(userRepository, new Argon2PasswordEncoder()));
//        UserDto userDto = new UserDto("Cpf Login", "iloveyou", "jane.doe@example.org", JwtRoleTypeEnum.ROLE_USER);
//
//        assertThrows(UnauthorizedOperationException.class,
//                () -> userController.update(userDto, new User("janedoe", "iloveyou", new ArrayList<GrantedAuthority>())));
    }

    @Disabled("Não foi possível fazer o teste da forma correta.")
    @Test
    void testRemove() throws UnauthorizedOperationException, UserCpfLoginNotFoundException {
//        UserRepository userRepository = mock(UserRepository.class);
//        UserController userController = new UserController(new UserServiceImp(userRepository, new Argon2PasswordEncoder()));
//        assertThrows(UnauthorizedOperationException.class, () -> userController.remove("User Cpf Login",
//                new User("janedoe", "iloveyou", new ArrayList<GrantedAuthority>())));
    }

    @Disabled("Não foi possível fazer o teste da forma correta.")
    @Test
    void testRemove2() throws UnauthorizedOperationException, UserCpfLoginNotFoundException {
//        UserJwt userJwt = new UserJwt();
//        userJwt.setIdentificator(1);
//        userJwt.setPassword("iloveyou");
//        userJwt.setEmail("jane.doe@example.org");
//        userJwt.setRole("Role");
//        userJwt.setCpfLogin("Cpf Login");
//        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);
//        UserRepository userRepository = mock(UserRepository.class);
//        doNothing().when(userRepository).deleteById((Integer) any());
//        when(userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
//        UserController userController = new UserController(new UserServiceImp(userRepository, new Argon2PasswordEncoder()));
//        userController.remove("janedoe", new User("janedoe", "iloveyou", new ArrayList<GrantedAuthority>()));
//        verify(userRepository).deleteById((Integer) any());
//        verify(userRepository).findByCpfLogin(anyString());
    }

    @Disabled("Não foi possível fazer o teste da forma correta.")
    @Test
    void testSave() throws UnauthorizedOperationException, UserCpfLoginExistException {
//        UserJwt userJwt = new UserJwt();
//        userJwt.setIdentificator(1);
//        userJwt.setPassword("iloveyou");
//        userJwt.setEmail("jane.doe@example.org");
//        userJwt.setRole("Role");
//        userJwt.setCpfLogin("Cpf Login");
//        UserRepository userRepository = mock(UserRepository.class);
//        when(userRepository.findByCpfLogin(anyString())).thenReturn(Optional.<UserJwt>of(userJwt));
//        UserController userController = new UserController(new UserServiceImp(userRepository, new Argon2PasswordEncoder()));
//        UserDto userDto = new UserDto("Cpf Login", "iloveyou", "jane.doe@example.org", JwtRoleTypeEnum.ROLE_USER);
//
//        userController.save(userDto, new User("janedoe", "iloveyou", new ArrayList<GrantedAuthority>()));
//        verify(userRepository).findByCpfLogin(anyString());
    }
}

