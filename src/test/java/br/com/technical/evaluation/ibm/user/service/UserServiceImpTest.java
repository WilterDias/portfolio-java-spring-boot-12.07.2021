package br.com.technical.evaluation.ibm.user.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.technical.evaluation.ibm.config.JwtRoleTypeEnum;
import br.com.technical.evaluation.ibm.user.dtos.UserDto;
import br.com.technical.evaluation.ibm.user.dtos.UserSaveDto;
import br.com.technical.evaluation.ibm.user.dtos.UserUpdateDto;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginExistException;
import br.com.technical.evaluation.ibm.user.exceptions.UserCpfLoginNotFoundException;
import br.com.technical.evaluation.ibm.user.model.UserJwt;
import br.com.technical.evaluation.ibm.user.repository.UserRepository;

import java.util.ArrayList;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserServiceImp.class})
@ExtendWith(SpringExtension.class)
class UserServiceImpTest {
    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserServiceImp userServiceImp;

    private final String CPF = "22174278065";
    private final String ROLE_USER = "ROLE_USER";

    @Test
    void testLoadUserByUsername() throws UsernameNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        UserDetails actualLoadUserByUsernameResult = this.userServiceImp.loadUserByUsername(CPF);
        assertEquals(1, actualLoadUserByUsernameResult.getAuthorities().size());
        assertEquals(
                "org.springframework.security.core.userdetails.User [Username="+CPF+", Password=[PROTECTED], Enabled=true,"
                        + " AccountNonExpired=true, credentialsNonExpired=true, AccountNonLocked=true, Granted Authorities=["+ROLE_USER
                        + "]]",
                actualLoadUserByUsernameResult.toString());
        assertTrue(actualLoadUserByUsernameResult.isEnabled());
        assertTrue(actualLoadUserByUsernameResult.isCredentialsNonExpired());
        assertTrue(actualLoadUserByUsernameResult.isAccountNonLocked());
        assertTrue(actualLoadUserByUsernameResult.isAccountNonExpired());
        assertEquals(CPF, actualLoadUserByUsernameResult.getUsername());
        assertEquals("iloveyou", actualLoadUserByUsernameResult.getPassword());
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testLoadUserByUsername2() throws UsernameNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole("");
        userJwt.setCpfLogin(CPF);
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                this.userServiceImp.loadUserByUsername(CPF));
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testLoadUserByUsername3() throws UsernameNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin("");
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                this.userServiceImp.loadUserByUsername(CPF));
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testLoadUserByUsername4() throws UsernameNotFoundException {
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(Optional.<UserJwt>empty());
        assertThrows(UsernameNotFoundException.class, () -> this.userServiceImp.loadUserByUsername(CPF));
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testSave() throws UserCpfLoginExistException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        assertThrows(UserCpfLoginExistException.class, () -> this.userServiceImp
                .save(new UserSaveDto(CPF, "iloveyou", "jane.doe@example.org", JwtRoleTypeEnum.ROLE_USER)));
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testSave2() throws UserCpfLoginExistException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        when(this.userRepository.save((UserJwt) any())).thenReturn(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(Optional.<UserJwt>empty());
        when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
        UserSaveDto userSaveDto = new UserSaveDto(CPF, "iloveyou", "jane.doe@example.org", JwtRoleTypeEnum.ROLE_USER);

        this.userServiceImp.save(userSaveDto);
        verify(this.userRepository).findByCpfLogin(anyString());
        verify(this.userRepository).save((UserJwt) any());
        verify(this.passwordEncoder).encode((CharSequence) any());
        assertEquals("foo", userSaveDto.getPassword());
    }

    @Test
    void testSave3() throws UserCpfLoginExistException {
        UserJwt userJwt = new UserJwt(1, CPF, "iloveyou", "jane.doe@example.org", ROLE_USER);
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        when(this.userRepository.save((UserJwt) any())).thenReturn(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(Optional.<UserJwt>empty());
        when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
        UserSaveDto userSaveDto = mock(UserSaveDto.class);
        when(userSaveDto.getRole()).thenReturn(JwtRoleTypeEnum.ROLE_USER);
        when(userSaveDto.getEmail()).thenReturn("foo");
        doNothing().when(userSaveDto).setPassword(anyString());
        when(userSaveDto.getPassword()).thenReturn("foo");
        when(userSaveDto.getCpfLogin()).thenReturn("foo");
        this.userServiceImp.save(userSaveDto);
        verify(this.userRepository).findByCpfLogin(anyString());
        verify(this.userRepository).save((UserJwt) any());
        verify(this.passwordEncoder).encode((CharSequence) any());
        verify(userSaveDto, times(2)).getCpfLogin();
        verify(userSaveDto).getEmail();
        verify(userSaveDto, times(2)).getPassword();
        verify(userSaveDto).getRole();
        verify(userSaveDto).setPassword(anyString());
    }

    @Test
    void testGetByCpfLogin() throws UserCpfLoginNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        this.userServiceImp.getByCpfLogin(CPF);
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testGetByCpfLogin2() throws UserCpfLoginNotFoundException {
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(Optional.<UserJwt>empty());
        assertThrows(UserCpfLoginNotFoundException.class, () -> this.userServiceImp.getByCpfLogin(CPF));
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testListAll() {
        when(this.userRepository.findAll()).thenReturn(new ArrayList<UserJwt>());
        assertTrue(this.userServiceImp.listAll().isEmpty());
        verify(this.userRepository).findAll();
    }

    @Test
    void testListAll2() {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);

        ArrayList<UserJwt> userJwtList = new ArrayList<UserJwt>();
        userJwtList.add(userJwt);
        when(this.userRepository.findAll()).thenReturn(userJwtList);
        this.userServiceImp.listAll();
        verify(this.userRepository).findAll();
    }

    @Test
    void testUpdate() throws UserCpfLoginNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);

        UserJwt userJwt1 = new UserJwt();
        userJwt1.setIdentificator(1);
        userJwt1.setPassword("iloveyou");
        userJwt1.setEmail("jane.doe@example.org");
        userJwt1.setRole(ROLE_USER);
        userJwt1.setCpfLogin(CPF);
        when(this.userRepository.save((UserJwt) any())).thenReturn(userJwt1);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
        UserUpdateDto userUpdateDto = new UserUpdateDto(CPF,
                                                       "iloveyou",
                                                        "jane.doe@example.org",
                                                        JwtRoleTypeEnum.ROLE_USER);

        this.userServiceImp.update(userUpdateDto);
        verify(this.userRepository).findByCpfLogin(anyString());
        verify(this.userRepository).save((UserJwt) any());
        verify(this.passwordEncoder).encode((CharSequence) any());
        assertEquals("foo", userUpdateDto.getPassword());
    }

    @Test
    void testUpdate2() throws UserCpfLoginNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        when(this.userRepository.save((UserJwt) any())).thenReturn(userJwt);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(Optional.<UserJwt>empty());
        when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
        Assertions.assertThrows(UserCpfLoginNotFoundException.class, () ->
                this.userServiceImp.update(
                        new UserUpdateDto(CPF,
                        "iloveyou",
                                "jane.doe@example.org",
                                JwtRoleTypeEnum.ROLE_USER)));
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testUpdate3() throws UserCpfLoginNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);

        UserJwt userJwt1 = new UserJwt(1, CPF, "iloveyou", "jane.doe@example.org", ROLE_USER);
        userJwt1.setIdentificator(1);
        userJwt1.setPassword("iloveyou");
        userJwt1.setEmail("jane.doe@example.org");
        userJwt1.setRole(ROLE_USER);
        userJwt1.setCpfLogin(CPF);
        when(this.userRepository.save((UserJwt) any())).thenReturn(userJwt1);
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("foo");
        UserUpdateDto userUpdateDto = mock(UserUpdateDto.class);
        when(userUpdateDto.getRole()).thenReturn(JwtRoleTypeEnum.ROLE_USER);
        when(userUpdateDto.getEmail()).thenReturn("foo");
        doNothing().when(userUpdateDto).setPassword(anyString());
        when(userUpdateDto.getPassword()).thenReturn("foo");
        when(userUpdateDto.getCpfLogin()).thenReturn("foo");
        this.userServiceImp.update(userUpdateDto);
        verify(this.userRepository).findByCpfLogin(anyString());
        verify(this.userRepository).save((UserJwt) any());
        verify(this.passwordEncoder).encode((CharSequence) any());
        verify(userUpdateDto, times(2)).getCpfLogin();
        verify(userUpdateDto).getEmail();
        verify(userUpdateDto, times(2)).getPassword();
        verify(userUpdateDto).getRole();
        verify(userUpdateDto).setPassword(anyString());
    }

    @Test
    void testRemove() throws UserCpfLoginNotFoundException {
        UserJwt userJwt = new UserJwt();
        userJwt.setIdentificator(1);
        userJwt.setPassword("iloveyou");
        userJwt.setEmail("jane.doe@example.org");
        userJwt.setRole(ROLE_USER);
        userJwt.setCpfLogin(CPF);
        Optional<UserJwt> ofResult = Optional.<UserJwt>of(userJwt);
        doNothing().when(this.userRepository).deleteById((Integer) any());
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(ofResult);
        this.userServiceImp.remove(CPF);
        verify(this.userRepository).deleteById((Integer) any());
        verify(this.userRepository).findByCpfLogin(anyString());
    }

    @Test
    void testRemove2() throws UserCpfLoginNotFoundException {
        doNothing().when(this.userRepository).deleteById((Integer) any());
        when(this.userRepository.findByCpfLogin(anyString())).thenReturn(Optional.<UserJwt>empty());
        Assertions.assertThrows(UserCpfLoginNotFoundException.class, () ->
                this.userServiceImp.remove(CPF));
        verify(this.userRepository).findByCpfLogin(anyString());
    }
}

