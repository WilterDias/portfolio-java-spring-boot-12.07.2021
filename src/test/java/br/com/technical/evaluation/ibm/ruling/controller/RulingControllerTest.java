package br.com.technical.evaluation.ibm.ruling.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import br.com.technical.evaluation.ibm.ruling.dtos.RulingDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingSaveDto;
import br.com.technical.evaluation.ibm.ruling.dtos.RulingUpdateDto;
import br.com.technical.evaluation.ibm.ruling.service.RulingService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;

import java.util.ArrayList;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {RulingController.class})
@ExtendWith(SpringExtension.class)
class RulingControllerTest {
    @Autowired
    private RulingController rulingController;

    @MockBean
    private RulingService rulingService;

    @Test
    void testGetById() throws Exception {
        when(this.rulingService.getById((Integer) any())).thenReturn(Optional.<RulingDto>of(new RulingDto()));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/rulings/{rulingId}", 123);
        MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content()
                        .string("{\"identificator\":null,\"title\":null,\"description\":null,\"documentsUrl\":null,\"sessionStartDate\":null,\"sessionDurationInMinutes\":null,\"voteCountYes\":null,\"voteCountTotal\":null}"));
    }

    @Test
    void testListAll() throws Exception {
        when(this.rulingService.listAll()).thenReturn(new ArrayList<RulingDto>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/rulings/list");
        MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Test
    void testListAll2() throws Exception {
        when(this.rulingService.listAll()).thenReturn(new ArrayList<RulingDto>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/rulings/list");
        getResult.contentType("Not all who wander are lost");
        MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Test
    void testRemove() throws Exception {
        doNothing().when(this.rulingService).remove((Integer) any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/rulings/{rulingId}", 123);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void testRemove2() throws Exception {
        doNothing().when(this.rulingService).remove((Integer) any());
        MockHttpServletRequestBuilder deleteResult = MockMvcRequestBuilders.delete("/api/rulings/{rulingId}", 123);
        deleteResult.contentType("Not all who wander are lost");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(deleteResult);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void testSave2() throws Exception {
        when(this.rulingService.save((RulingSaveDto) any())).thenReturn(Optional.<RulingDto>of(new RulingDto()));

        RulingSaveDto rulingSaveDto = new RulingSaveDto();
        rulingSaveDto.setDocumentsUrl("https://example.org/example");
        rulingSaveDto.setSessionStartDate(null);
        rulingSaveDto.setTitle("Dr");
        rulingSaveDto.setSessionDurationInMinutes(1);
        rulingSaveDto.setDescription("The characteristics of someone or something");
        String content = (new ObjectMapper()).writeValueAsString(rulingSaveDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/rulings/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"identificator\":null,\"title\":null,\"description\":null," +
                                        "\"documentsUrl\":null,\"sessionStartDate\":null," +
                                        "\"sessionDurationInMinutes\":null," +
                                        "\"voteCountYes\":null,\"voteCountTotal\":null}"));
    }

    @Test
    void testSave() throws Exception {
        RulingSaveDto rulingSaveDto = new RulingSaveDto();
        rulingSaveDto.setDocumentsUrl("https://example.org/example");
        rulingSaveDto.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        rulingSaveDto.setTitle("Dr");
        rulingSaveDto.setSessionDurationInMinutes(1);
        rulingSaveDto.setDescription("The characteristics of someone or something");
        String content = (new ObjectMapper()).writeValueAsString(rulingSaveDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/rulings/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void testUpdate() throws Exception {
        RulingUpdateDto rulingUpdateDto = new RulingUpdateDto();
        rulingUpdateDto.setDocumentsUrl("https://example.org/example");
        rulingUpdateDto.setIdentificator(1);
        rulingUpdateDto.setSessionStartDate(LocalDateTime.of(1, 1, 1, 1, 1));
        rulingUpdateDto.setTitle("Dr");
        rulingUpdateDto.setSessionDurationInMinutes(1);
        rulingUpdateDto.setDescription("The characteristics of someone or something");
        String content = (new ObjectMapper()).writeValueAsString(rulingUpdateDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/rulings/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.rulingController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }
}

