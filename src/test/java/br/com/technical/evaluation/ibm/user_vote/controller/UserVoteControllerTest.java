package br.com.technical.evaluation.ibm.user_vote.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import br.com.technical.evaluation.ibm.config.JwtServiceConfig;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteForListDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteRemoveDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteSaveDto;
import br.com.technical.evaluation.ibm.user_vote.dtos.UserVoteUpdateDto;
import br.com.technical.evaluation.ibm.user_vote.service.UserVoteService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {UserVoteController.class})
@ExtendWith(SpringExtension.class)
class UserVoteControllerTest {
    @Autowired
    private UserVoteController userVoteController;

    @MockBean
    private UserVoteService userVoteService;

    @MockBean
    private JwtServiceConfig jwtServiceConfig;

    private final String CPF = "22174278065";
    private final String ROLE_USER = "ROLE_USER";

    @Disabled("Não foi possível fazer o teste da forma correta.")
    @Test
    void testGetByCpfLoginAndRulingId() throws Exception {
//        when(this.userVoteService.getByCpfLoginAndRulingId(anyString(), (Integer) any())).
//                thenReturn(Optional.of(new UserVoteDto()));
//        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/uservotes/byruling/{rulingId}",
//                123);
//        MockMvcBuilders.standaloneSetup(this.userVoteController)
//                .build()
//                .perform(requestBuilder)
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
//                .andExpect(MockMvcResultMatchers.content().string("<Optional><rulingId/><voteIsYes/></Optional>"));
    }

    @Test
    void testListAllByRulingId() throws Exception {
        when(this.userVoteService.listAllByRulingId((Integer) any())).thenReturn(new ArrayList<UserVoteForListDto>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/uservotes/list/byruling/{rulingId}",
                123);
        MockMvcBuilders.standaloneSetup(this.userVoteController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Test
    void testListAllByRulingId2() throws Exception {
        when(this.userVoteService.listAllByRulingId((Integer) any())).thenReturn(new ArrayList<UserVoteForListDto>());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/uservotes/list/byruling/{rulingId}",
                123);
        getResult.contentType("Not all who wander are lost");
        MockMvcBuilders.standaloneSetup(this.userVoteController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Disabled("Não foi possível fazer o teste da forma correta.")
    @Test
    void testUpdate() throws Exception {
//        when(this.userVoteService.update(anyString(), (UserVoteUpdateDto) any())).thenReturn(Optional.of(new UserVoteUpdateDto()));
//        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/uservotes/",
//                123);
//        MockMvcBuilders.standaloneSetup(this.userVoteController)
//                .build()
//                .perform(requestBuilder)
//                .andExpect(MockMvcResultMatchers.status().isAccepted())
//                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
//                .andExpect(MockMvcResultMatchers.content().string("<Optional><cpfLogin/><rulingId/><voteIsYes/></Optional>"));
    }

    @Disabled("Não foi possível fazer o teste da forma correta.")
    @Test
    void testSave() throws Exception {
//        when(this.userVoteService.save(anyString(), (UserVoteSaveDto) any())).thenReturn(Optional.of(new UserVoteDto()));
//        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/uservotes/",
//                123);
//        MockMvcBuilders.standaloneSetup(this.userVoteController)
//                .build()
//                .perform(requestBuilder)
//                .andExpect(MockMvcResultMatchers.status().isCreated())
//                .andExpect(MockMvcResultMatchers.content().contentType("application/json;"))
//                .andExpect(MockMvcResultMatchers.content().string("<Optional><rulingId/><voteIsYes/></Optional>"));
    }

    @Test
    void testRemove() throws Exception {
        doNothing().when(this.userVoteService).removeByCpfLoginAndRulingId(anyString(), (Integer) any());

        UserVoteRemoveDto userVoteRemoveDto = new UserVoteRemoveDto();
        userVoteRemoveDto.setCpfLogin(CPF);
        String content = (new ObjectMapper()).writeValueAsString(userVoteRemoveDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/api/uservotes/byruling/{rulingId}", 123)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userVoteController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
