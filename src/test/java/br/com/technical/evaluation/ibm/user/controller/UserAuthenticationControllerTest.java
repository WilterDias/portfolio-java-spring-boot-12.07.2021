package br.com.technical.evaluation.ibm.user.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

import br.com.technical.evaluation.ibm.user.dtos.UserAuthenticationRequestDto;
import br.com.technical.evaluation.ibm.user.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@Disabled("Não foi possível fazer os testes da forma correta.")
@ContextConfiguration(classes = {UserAuthenticationController.class, ApplicationContext.class})
@ExtendWith(SpringExtension.class)
class UserAuthenticationControllerTest {
    @MockBean
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserAuthenticationController userAuthenticationController;

    @MockBean
    private UserService userService;

    @Test
    void testGenerateToken() throws Exception {
        // TODO: Falhou ao criar Spring context.

        when(this.userService.loadUserByUsername(anyString())).thenThrow(new BadCredentialsException("Msg"));
        when(this.authenticationManager.authenticate(any()))
                 .thenReturn(new TestingAuthenticationToken("Principal", "Credentials"));

        UserAuthenticationRequestDto userAuthenticationRequestDto = new UserAuthenticationRequestDto();
        userAuthenticationRequestDto.setPassword("iloveyou");
        userAuthenticationRequestDto.setCpfLogin("Cpf Login");
        String content = (new ObjectMapper()).writeValueAsString(userAuthenticationRequestDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.userAuthenticationController).build().perform(requestBuilder);
    }

    @Test
    void testGenerateToken2() throws Exception {
        // TODO: Falhou ao criar Spring context.

        when(this.userService.loadUserByUsername(anyString())).thenThrow(new UsernameNotFoundException("Msg"));
        when(this.authenticationManager.authenticate((org.springframework.security.core.Authentication) any()))
                .thenReturn(new TestingAuthenticationToken("Principal", "Credentials"));

        UserAuthenticationRequestDto userAuthenticationRequestDto = new UserAuthenticationRequestDto();
        userAuthenticationRequestDto.setPassword("iloveyou");
        userAuthenticationRequestDto.setCpfLogin("Cpf Login");
        String content = (new ObjectMapper()).writeValueAsString(userAuthenticationRequestDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.userAuthenticationController).build().perform(requestBuilder);
    }

    @Test
    void testGenerateToken3() throws Exception {
        // TODO: Falhou ao criar Spring context.

        when(this.userService.loadUserByUsername(anyString())).thenThrow(new DisabledException("Msg"));
        when(this.authenticationManager.authenticate((org.springframework.security.core.Authentication) any()))
                .thenReturn(new TestingAuthenticationToken("Principal", "Credentials"));

        UserAuthenticationRequestDto userAuthenticationRequestDto = new UserAuthenticationRequestDto();
        userAuthenticationRequestDto.setPassword("iloveyou");
        userAuthenticationRequestDto.setCpfLogin("Cpf Login");
        String content = (new ObjectMapper()).writeValueAsString(userAuthenticationRequestDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.userAuthenticationController).build().perform(requestBuilder);
    }

    @Test
    void testRestoretoken() throws Exception {
        // TODO: Falhou ao criar Spring context.

        SecurityMockMvcRequestBuilders.FormLoginRequestBuilder requestBuilder = SecurityMockMvcRequestBuilders.formLogin();
        MockMvcBuilders.standaloneSetup(this.userAuthenticationController).build().perform(requestBuilder);
    }
}

