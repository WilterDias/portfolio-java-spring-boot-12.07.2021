# Avaliação técnica IBM - JAVA

Esse projeto é o resultado de um desafio promovido na seleção da IBM. Visa a construção de uma API REST
em JAVA para ser utilizada por um cliente em dispositivo móvel.
A aplicação pode ser executada em nuvem, no entanto todo o processo de desenvolvimento foi feito
localmente.

Foi desenvolvido as novas funcionalidades de acordo com o que foi entendido nas instruções fornecidas 
em arquivo (olhar [Avaliação técnica](AvaliacaoTecnicaDev-2.docx)).


# Objetivos gerais atingidos
- Utilização de boas práticas da API REST, principalmente relacionadas ao padrão dos endpoints.
- Criação de uma API utilizando a estrutura base do spring-boot.
- Criação cobertura de testes unitários para as principais funcionalidades não complexas.
- Configuração do Swagger para a documentação da API, acessar a URL `http://localhost:8080/swagger-ui.html`.
- Persistência em banco de dados Postgres SQL.
- Utilização de migrations com o Flyway.
- Padronizações de classe, atributos e métodos.
- Análise com o SonarLint para a descoberta de code smells, possíveis bugs, cobertura, vulnerabilidades e boas praticas.
- Autenticação de usuário.
- Configuração para internacionalização de mensagens (I18N).
- Utilização do GIT.
- Testes manuais com o Postman.


# Objetivos específicos atingidos

- CRUD da pauta (Ruling). Visualização da quantidade na pauta votos e limitação de tempo.
- CRUD do usuário (User).
- CRUD para o voto do usuário (UserVote).
- Verificação da validade do CPF por um servidor mockado, no momento da votação.
- Persistência em banco de dados.
- Versionamento da API, tanto em relação ao código por meio do GIT, como em relação 
  ao banco de dados por meio dos migrations.


# Possíveis melhorias
- Não foram feitos testes de perfomance. No entanto se houvesse mais tempo seria possível
  a configuração de um servidor de mensageria para evitar problemas quando se recebe centenas
  de milhares de votos. Para isso poderia ser utilizado o Kafka.
- Por mais que tenha sido feitos os testes unitários, faltou ter mais cobertura, o motivo de não ter
  tido mais cobertura foi a complexidade de se testar alguns métodos, os quais dependem de parâmetros
  advindo da autenticação.
- Criar uma constraint para verificar o CPF.
- Criar um HandlerException para interceptar as exceções.
- A geração dos logs foi feita em alguns pontos, no entanto seria interessante um Interceptor
  apenas para fazer os logs automaticamente quando houver alguma requisição, ou quando houver algum
  erro na requisição.

# Instruções

## Requerimentos
* Java 11+
* Maven
* PostgreSQL
* SoapUI (Mockar o servidor de verificação de CPF).

# Execução do projeto
- Foi utilizada a IDE Intellij.
- Necessário instalar o plugin do Lombok na IDE, e habilitar o `Enable annotation processing`.

# Caminho Feliz
Os caminhos felizes nada mais são que os caminhos prováveis e de maiores usos, e que devem funcionar.
O caminho feliz principal é:
- Levantar a aplicação e o servidor mockado de verificação de CPF.
- Cadatrar um usuário pelo menos. Somente administradores podem cadastrar administradores, 
 a aplicação quando implantada gera um administrador padrão, login= `22174278065`, e senha = `admin`.
- Autenticar o usuário criado e guardar o token para ser usado.
- Criar uma pauta. Somente administradores podem criar pauta.
- Listar as pautas e escolher uma.
- Votar na pauta escolhida.
- Obter a pauta escolhida para verificar a contagem de votos.

# Observações
- O postman foi utilizado para testar manualmente, os arquivos utilizados e as variáveis de ambiente
  se encontra [arquivos para o postman](./src/main/resources/postman.requests.test).
- No momento da votação, há uma variável booleana para verificar se o voto é sim (yes) quando true, 
  ou não (no) quando false. A quantidade de voto não (no) é possível ser informada por complementariedade
  (Quantidade total de votos menos a quantidade de votos sim).
- Caso o servidor de verificação de CPF não exista ou não haja comunicação, a verificação será ignorada.
  O servidor pode ser mockado fazendo uso dos [arquivos do SoapUI](./src/main/resources/required.use.soapui.mock).
  O servidor nega somente a votação para o CPF `22174278066`.
- Existem vários //TODO no projeto para mostrar o que poderia ser melhorado, ou para avisar algo.

## Variavéis de ambiente
* POSTGRE_URL - Atalho para `spring.datasource.url`
* POSTGRE_PORT - Atalho para `spring.datasource.url`
* POSTGRE_DB - Atalho para `spring.datasource.url`
* POSTGRE_USER - Atalho para `spring.datasource.username`
* POSTGRE_PASS - Atalho para `spring.datasource.password`
* EXECUTE_MIGRATIONS - Atalho para `spring.flyway.enabled`.
* SWAGGER_HOST - Host para o Swagger 2.0, formato do valor `http://< URL >:< PORT >`.
* CPF_CHECKER_HOST - Host para o checador de CPF, formato do valor `http://< URL >:< PORT >`.

### Docker
É possível gerar a imagem do docker, o comando encontra-se no arquivo a seguir.
O JIB foi utilizado para permitir a tarefa.

```bash
$ ./build.sh docker
```

### Docker Remote

```bash
docker run -d \
    <URL>:<PORT>/<PATH>/prova-java-api:TAG
```

# Agradecimentos e créditos
* O uso do pacote JJWT e a implementação do JWT foram
  com base no tutorial de JavaInUse, disponível em
  `https://www.javainuse.com/webseries/spring-security-jwt/chap7,
  acessado em 12/07/2021`.